## Version 2.0
## language: en

Feature: 215-steganography-ringzeroctf
  Site:
    https://ringzer0ctf.com/challenges/24
  Category:
    Steganography
  User:
    SullenestDust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Arch Linux      | 5.7.7-arch1-1 |
    | Chrome          | 85.0.4181.8   |
  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/215
    """
    When I open the URL with Google Chrome
    Then I see the challenge statement
    """
    There's something suspicious about this letter I've been reading.
    I think it contain a secret message, can you find it?
    """
    And I download a file
    """
    93af48d6653be40a1919484a7481ddc4.zip
    """
    Given the file
    When I extract the file contents
    Then I found a text file
    """
    LoveLetter.txt
    """

  Scenario: Fail: Check Encrypted or hidden file
    Given The text file
    When I open it
    Then I Found what it looks like a letter
    And I decide that the answer is not in plain view
    Given the file
    When I try to check the file type with
    """
    exiftool
    """
    Then I found that is indeed a text file
    And that has an encoding
    """
    iso-8859-1
    """
    Then to ckeck out if the file is not encrypted, i use
    """
    https://gchq.github.io/CyberChef/
    """
    And Found a normal entropy for an english text
    Given That result, I conclude that is not an encrypted file
    When I encode and decode the file using different encodings with
    """
    https://gchq.github.io/CyberChef/
    """
    Then I found no new information
    When I check the file in hex using
    """
    https://gchq.github.io/CyberChef/
    """
    Then I found nothing interesting

  Scenario: Fail: Check permutations of letters
    Given The text file
    When I opened with a python script and read its contents
    Then I try to create a message with first letters of every line
    And found nothing relevant
    Given that result
    When I try to create a message with last letters of every line
    Then found nothing useful
    When I try to create a message with first and last letters of every line
    Then found not useful information
    When I try different combination with letters and words
    Then I still don't get relevant information

  Scenario: Success: Check spaces
    Given The file
    """
    LoveLetter.txt
    """
    When I check it more thoroughly
    Then I notice, that there are some weird spaces between some words
    """
    ⍽ ⍽ ⍽
    """
    And I look and discover that they are non-breaking spaces
    Given That those spaces look too suspicious
    When I procede to count them and normal spaces too
    Then I found to numbers but don't think they are useful
    Given That i have those two space types
    When I try to create a binary message with ones for normal spaces
    And zeroes for the other spaces
    Then I don't get a useful message when i transform to decimal
    When I try to create a binary message with zeroes for normal spaces
    And ones for the other spaces
    Then I get the flag when i transform the message to decimal
    Then I found the flag
