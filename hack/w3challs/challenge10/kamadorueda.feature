Feature: Solve the challenge A crackme or ...
  From www.w3challs.com
  With my username kedavamaru

  Background:
    Given an executable file downloaded from the challenge site

  Scenario: Succesful Solution
    When I open the file with an hex editor
    And I interpret each byte with the ISO 8859-1 standard
    And I interpret from the byte 10781 to the byte 10930 inclusive
    Then I get a string in french
    # Some special characters have been removed for compatibility reasons
    """
    He ben Bravo, je sais, c'est trop facile, t'inquiete pas,
    ca va se corser petit a petit... allez, le mot de passe
    pour valider est: tjrregarderlasrc
    """
    When I use tjrregarderlasrc as password
    Then I solve the challenge
