#!/usr/bin/env python
# pylint: disable=no-member
# pylint: disable=F0401
"""
$ pylint skhorn.py
No config file found, using default configuration
Your code has been rated at 10.00/10 (previous run: 8.49/10, +1.51)
$ chmod +x skhorn.py
$
"""
import io
import pytesseract
import requests
from PIL import Image
from bs4 import BeautifulSoup


# URL's array
URL = [
    "https://w3challs.com/challs/Prog/Ep4/"
]
# PHP pages array
PHP_PAGE = ['chuck.php']
# Cookies dict
COOKIES = {
    '_utma': '265617161.852701107.1516137379.1523536336.1523543458.91',
    '_utmb': '265617161.2.10.1523543458',
    '_utmc': '265617161',
    '_utmz': '265617161.1523023215.87.3.utmcsr=google|utmccn=(organic)|\
              utmcmd=organic|utmctr=(not%20provided)',
    'lang': 'en',
    'nick': 'Skhorn',
    'session': 'a61e2eadfa25c02ec4d95fbeb8635cc64017015c',
    'lock_menu': '1',
    'PHPSESSID': 'qk4c9e0udaj0mdk5nqolmlp231',
}

PAYLOAD = {}
PROXIES = {}


def connect(url, cookie, payload, proxies):
    """
    Send POST req, with cookie, payload + tell
    requests not to verify SSL(CA_certificates)
    """
    response = requests.post(url,
                             cookies=cookie,
                             data=payload,
                             proxies=proxies,
                             verify=False).text
    return response


def search_in_html(response, tag):
    """
    Search in response, any tag given and return
    all tag encountered
    """
    soup_parser = BeautifulSoup(response, 'html.parser')

    return soup_parser.findAll(tag)


def main():
    """
    https://w3challs.com/challenges/challenge40
    Script to solve timming challenge
    """
    # First connection within the challenge
    response = connect(URL[0]+PHP_PAGE[0],
                       COOKIES,
                       payload="",
                       proxies="")
    print response

    # Looking for any img tag
    html_search = search_in_html(response, 'img')
    # print html_search
    # Get the src on index '1', that's were the image
    # of the challenge is.
    image = html_search[1].get("src")
    # Create the entire url image
    image = URL[0]+image

    # Get the image via get sending all cookies
    response = requests.get(image, cookies=COOKIES)
    # Usage of io and PIL to process the image
    # using the response.content as parameter
    img = Image.open(io.BytesIO(response.content))
    # Let pytesseract works on the bytes generated
    text = pytesseract.image_to_string(img)
    # Magick begins
    # Split unicode data
    data = text.split()
    # print data
    # Separate them
    pow_data = data[0]
    x_value, y_value = "", ""
    # Replace \u2018 for other character
    # And encode result in ascii + split
    pow_data = pow_data.replace(u"\u2018", " ")
    pow_data = pow_data.encode('ascii').split(' ')
    # This way we can get both values by a simple replace
    x_value = pow_data[0].replace('(', '')
    y_value = pow_data[1].replace(')', '')
    # Encode data[2] to get last value
    z_value = data[2].encode('ascii')
    print "X:::{0} Y:::{1} Z:::{2}".format(x_value, y_value, z_value)

    # Use built-in function to operate
    # (x ^ y) mod z
    result = pow(int(x_value), int(y_value), int(z_value))
    # print result

    # Generate payload dit
    payload = {'answer': result}
    # Send answer
    response = connect(URL[0]+PHP_PAGE[0],
                       COOKIES,
                       payload,
                       proxies="")
    print "==============================================="
    print "FIRST STAGE RESPONSE", response
    print "==============================================="
    proxies = {'https': 'https://151.80.140.233:54566'}

    # POST REQUEST
    response = connect(URL[0]+PHP_PAGE[0],
                       COOKIES,
                       payload,
                       proxies)
    print "==============================================="
    print "SECOND STAGE RESPONSE", response
    print "==============================================="


main()
