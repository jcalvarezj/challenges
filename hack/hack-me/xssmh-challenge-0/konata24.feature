## Version 2.0
## language: en

Feature: xss-Hack-me
  Site:
    Hack-me
  Category:
    XSS
  User:
    konata24
  Goal:
    Make an alert

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing the website
    Then I choose a challenge
    And I start the virtual machine

  Scenario: Fail:type-alert-in-the-page
    Given A page that has a text field and a button
    When I try to type "alert(Hello, world)" in the text field
    Then I press the button "Inject!"
    And I see a message that says "alert(Hello, world)"
    When I see that I think that the alert doesn't work
    And I fail to get the flag

  Scenario: Success:type-alert-in-script-tags
    Given The page
    When I try to type "<script>alert('Hello, world')</script>"
    Then I submit the page
    And I see an alert with a message that says "Hello, world"
    When I see the result
    And I capture the flag
