## Version 2.0
## language: en

Feature: CHALLENGE-Hack.me
  Site:
    Hack.me
  Category:
    SQL injection
  User:
    slayfer1112
  Goal:
    Bypass the authentication

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    Then Start the virtual machine on the website
    And The website ask for a user and password to continue

  Scenario: Fail:Type-a-generic-user-and-password
    Given I'm on the fields that ask for a user and password
    When I try some generic users and passwords like "admin", "admin123"
    Then I see a message that say "incorrect usarname/password"[evidence](img1)
    And I fail to bypass the login

  Scenario: Fail:Inspect-code-to-see-the-script
    Given I'm on the field that ask me the username and password
    When I Inspect the page to see the HTML
    Then I see a function in the submit button "login();" [evidence](image2)
    And I search the function in the HTML
    When I found the function I see the following lines
    """
    function login() {
      var us = $("#username")[0].value;
      var pwd = $("#password")[0].value;
      var ajaxRequest = $.ajax({
        url : "login.php",
        type : "POST",
        data : {
          username : us,
          password : pwd
        },
        dataType : "text",
        success : function(response) {
          $("#responseBody").text(response);
          var str=response.substr(0,13);
          if (str=="Correct login") {
            document.location.href='private.php';
          }
        },
        error : function(error) {

        }
      });
    }
    """
    Then I see that this function take the username and password
    And I see that send that how a POST request to "login.php"
    When I read more, I see that eval the response of "login.php"
    Then I see that the response is text in the page
    And If the response is "Correct login" redirects to "private.php"
    When I found that I try to change "index.php" to "private.php"
    Then I was redirect to "index.php"
    And I fail to bypass the login

  Scenario:Success:SQL-payloads-in-POST-request
    Given I'm on the field that ask me the username and password
    Given I founde the script "login()"
    When I think about that I try to change "index.php" to "login.php"
    Then I found a message that say "Missing username"
    And I remember that the function send a POST request with some data
    When I try to use that data in the url like "?username=&password="
    Then I try with a generic user and password
    And I see a response with a meesage that say "Incorrect username/password"
    When I see that I try to inject some SQL payloads to bypass the auth
    Then I think that the SQL sintax is the following
    """
    SELECT <> FROM <users table> WHERE username='<username>'
                                   AND password='<password>'
    """
    And I try using the following payload "'OR 1 = 1 -- "
    When I use the payload and send the request I get a response
    Then I see that the response message is "Correct loginCiro"
    And I go to "private.php" again
    When I'm redirected to "private.php" I see a congrats message
    Then I see some images [evidence](img3)
    And I bypass the login auth
    And I found the flag
