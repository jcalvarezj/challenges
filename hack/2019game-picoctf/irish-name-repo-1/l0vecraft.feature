## Version 2.0
## language: en

Feature: Irish-Name-Repo-1 - web - picoCTF
  Code:
    Irish-Name-Repo-1
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name>    | <Version>              |
    | Ubuntu             | 18.04                  |
    | FireFox            | 72.0.1(64 bits)        |
  Machine information:
    Given I access to the challenge via browser
    When I enter to the challenge see a lot of photographs and a sidemenu
    Then I see a support item menu and a login item menu
    And the other options of the pagen doesn't work
    When I enter to the login option, I see a little form
    Then I can assume the the challenge consist in bypass this login

  Scenario: Success: Trying a simple Bypass
    When I enter in the platform, I go to the support page
    Then I see a few usernames like "Anna" and "admin"
    When I enter in the login page
    And I try a simple bypass with the username "admin"
    """
    admin'OR 1=1 --
    """
    Then I hit enter
    And the flag appears to me
    And now I can solve the challenge
