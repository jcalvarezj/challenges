## Version 2.0
## language: en

Feature: 712-H4CK1NG-enigmes-a-thematiques
  Code:
    712
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing the challenge page

  Scenario: Fail:search-for-keywords
    Given I am on the main page
    And I can see the message
    """
    Le but de cette énigme est de se connecter en admin sur la page suivante :
    index.php. Inutile de bruteforcer sur cette énigme, le mot de passe de
    l'admin est bien trop long et trop compliqué. En fait, il y a une faille
    de conception dans le code, à vous de la trouver !
    """
    When I search for "admin" into the source code
    Then I find a related code with a possible "<FLAG>"
    """
    if($entry[0]=='admin') {
      echo ('Bravo, le mot de passe pour valider l\'épreuve est xxxxxxx');
      die();
    }
    """
    When I set the possible "<FLAG>" to pass the challenge
    Then The possible "<FLAG>" does not work
    And I can not capture the flag

  Scenario: Success:find-design-error
    Given I am on the main page
    When I register a new user
    Then The website saves the username and the password inline into a TXT file
    And The information is joined with "||"
    """
    fwrite($db,$_POST['login'].'||'.$_POST['passwd'].'||'."\r\n");
    """
    When I login with the new username
    Then The website looks at the two first words of each line
    """
    if ($entry[0]==$_POST['login'] && $entry[1]==$_POST['passwd'])
    """
    And It checks if the username is "admin"
    """
    if($entry[0]=='admin')
    """
    When I register a new user with "admin||1234" as username
    Then Appears the login page
    When I login with "admin" as username and "1234" as password
    Then I can see a new message with the "<FLAG>"
    """
    Bravo, le mot de passe pour valider l'�preuve est <FLAG>.
    """
    When I submit the "<FLAG>" to pass the challenge
    Then I capture the flag
