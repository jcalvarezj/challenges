# language: en

Feature: Solve Encryption challenge 9
  From site Valhalla
  From Encryption Category
  With my username Skhorn

  Background:
    Given the fact i have an account on Valhalla site
    And i have Debian 9 as Operating System
    And i have internet access

  Scenario: Succesful Solution
    Given the link to the challenge
    And a plain text message
    And an equation to cipher the text
    And i see it applies module 26
    Then i code a script to help me with this
    And knowing the fact that i need to use the index of each char on the alphabet
    And use the equation "E(x) = 3x + 6 % 26"
    When i use the script to cipher the text
    And i get a set of characters
    And i use this as the answer
    Then i solve the challenge
