# language: en

Feature: Solve the challenge Try to see 2
  From the happy-security.de website
  From the JavaScript category
  With my username raballestas

  Background:
    Given an input

  Scenario: Successful solution
    When I look into the source code
    Then I see the submit calls a javascript function check()
    When I look into the code for the function check()
    Then I see a var pw which is document.title
    And a var p2 which takes some letters from pw
    Then I find the value of p2 using the console
    Then I write the value of pwd in the input
    And I solve the challenge
