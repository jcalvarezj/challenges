## Version 2.0
## language: en

Feature: Training programing
  Site:
    www.wechall.net
  Category:
    training
    coding
  User:
    karlhanso82
  Goal:
    Send back the same message
    given by the page
    before 1.337 seconds

 Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 10           |
     | Chromium        | 74.0.3729.131|

 Machine information:
    Given the challenge URL
    """
    https://www.wechall.net/challenge/training/programming1/index.php
    """
    Then I open the url with Chromium
    And I see the the challenge with a brief description
    """
    When you visit this link you receive a message.
    Submit the same message back to
    https://www.wechall.net/en/challenge/training/
    programming1/index.php?answer=the_message
    Your timelimit is 1.337 seconds
    """
    Then I click the link
    And gives me message
    Then I concluded that I must send this message again

  Scenario: Success:insert-js-code-in-browser-console
    And I open the web browser console
    Then I insert the following code
    """
    let typerequest = 'get';
    let url  = 'https://www.wechall.net/challenge/training'
    +'/programming1/index.php?action=request';

    let typedata = "text";
    let urlnew   = "index.php?answer=";
    jQuery.ajax({
      url:url,
      type: typerequest,
      dataType: typedata,
      success:function(data){
        var newUrl = urlnew + data;
        window.location.href = newUrl;
      }
     });
    """
    Then the code makes a GET request async call given link
    And waits for its succesfull reponse
    Then since the response is the message that we must send again
    And herein we add this message to the newurl
    Then windows.location.href property reload the page with this new value
    And I solve the challenge

