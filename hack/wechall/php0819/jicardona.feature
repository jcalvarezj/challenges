# language: en

Feature: Solve PHP 0819 challenge
  From the WeChall site
  Of the category PHP
  As the registered user disassembly

  Background:
    Given a highlighted version of the source code
    And some hints posted in the forums

  Scenario: Try the straightforward solution
    Given I have the final condition to solve the challenge
    And I can sent data throught the url with the ? char
    And must comply with the following restrictions:
    """
    5   $f = Common::getGetString('eval');
    6   $f = str_replace(array('`', '$', '*', '#', ':', '\\', '"', "'", '(', ')', '.', '>'), '', $f);
    8   if((strlen($f) > 13) || (false !== stripos($f, 'return')))
    9   {
    10    die('sorry, not allowed!');
    11  }
    13  try
    14  {
    15    eval("\$spaceone = $f");
    16  }
        catch (Exception $e)
        {
          return false;
        }
    22  return ($spaceone === '1337');
    """
    Then I enter the url with the fields: /index.php?eval=1337;
    But doesn't show me any errors since it might fail in one of the conditions
    And mostlikely has to be the identity comparison

  Scenario: Search information about string literals
    Given someone in the forums mention the string literals
    And I search information about how to compose a string
    Then found that a string can be specified in four diferent ways
    And I realize that simple and double qoutes are prohibited by the line 6
    But the last two options (heredoc and nowdoc) meet the conditions

  Scenario: Online testing the scripts
    Given the http://sandbox.onlinephpfunctions.com, I test:
    """
    #!/usr/bin/env php
    1  <?php
    2  $f = (string)"1337;";
    3  echo gettype($f);
    4  eval("\$spaceone = $f");
    5  echo gettype($spaceone);
    6  echo ($spaceone === '1337') ? 'true' : 'false';
    """
    Then I get the output: 3->string, 5->integer, 6->false
    Then I know that the main problem is in the type manipulation
    And it happends when the script calls the eval function
    Then I change the string literal using heredoc
    """
    2  $f = (string)"<<<S\n1337\nS;\n"
    """
    And finally the last comparison returns me true
    Then check the length of $f and gave me 13 chars since \n represent one

  Scenario: Solving the challenge
    Given I enter the url with the fields:
    """
    /index.php/index.php?eval=<<<S\n1337\nS;\n;
    """
    But it gave me an error due to backslashes are removed in line 6
    Then I replace those new lines to percentage url encoding
    """
    /index.php?eval=<<<S%0A1337%0AS;%0A
    """
    And finally solve the challenge
