## Version 2.0
## language: en

Feature: Warchall-WeChall
  Site:
    www.wechall.net
  Category:
    Warchall
  User:
    yamuz0
  Goal:
    To get access as level8 on Warchall server through a SSH key.

  Background:
  Hacker's software:
    | <Software name> | <Version>                 |
    | Ubuntu          | 16.04.5 LTS (amd64)       |
    | OpenSSH         | 7.2p2                     |
  Machine information:
    Given the challenge webpage indicates a remote log in on the Warchall server
    And from previous Warchall challenges I know the connection port is 19198
    And I already have the user "yamuz0" on the domain server
    Then I can access with "yamuz0" to get the SSH key that I need

  Scenario: Fail:Using in plain text a found RSA public key
    Given I already have access as "yamuz0" on the server by typing:
    """"""
    $ ssh -p 19198 yamuz0@warchall.net
    """"""
    And just when I get access I list the files with "ls -a" command
    And I see a WELCOME.txt file
    Then I look for its content with cat command
    And there I am told I could look for answers at /home/level
    When I am at /home/level I list the files and I see a "8" folder
    And due I need to log in as level8, I know I am at the right folder
    And there the only file with read permission is authorized_keys.backup
    Then I see its content and I realized is a RSA public key
    When I try to get access as level8 using authorized_keys.backup as password
    Then I get a permission denied message

  Scenario: Success:Private key from the found RSA public key
    Given that from the previous scenario I already have the RSA public key
    Then I can get the key footprint by doing:
    """"""
    $ ssh-keygen -l -f authorized_keys.backup -E md5
    """"""
    And I get the key footprint: 2b:cd:07:a7:01:e9:4a:04:74:d7:7e:e4:d6:d0:f8:06
    Then I can get the private key through either brute force or a database
    And I find a database that contains well-known PRNG vulnerabilities
    And this is quite probable the kind of vulnerability here
    And a link to that database and further explanation is on the repository:
    """"""
    https://github.com/g0tmi1k/debian-ssh
    """"""
    Then I only need to download the private key associated with the footprint
    And I try to log in as level8 using the donwloaded private key:
    """"""
    $ ssh -i /home/osboxes/Downloads/rsa/2048/2bcd07a701e94a0474d77ee4d6d0f806-2
    3669 -p 19198 level8@warchall.net
    """"""
    And I get the answer:
    """"""
    Your solution to level 8 is: PrivateKeysAreGold
    """"""
    And the connection is closed automatically afterwards
    Then I type "PrivateKeysAreGold" as solution on the webpage challenge
    And I solve the challenge
