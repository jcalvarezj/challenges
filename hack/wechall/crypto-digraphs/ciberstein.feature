Feature:
  Solve Crypto-Diagraphs
  From wechall site
  Category Crypto, Training

Background:
  Given I am running Windows 7 (64bit)
  And I am using Google Chrome Version 70.0.3538.77 (Official Build) (64-bit)

Scenario:
  Given the challenge URL
  """
  http://www.wechall.net/challenge/training/crypto/digraph/index.php
  """
  Then I opened that URL with Google Chrome
  And I see the problem statement
  """
  This time I am using a digraph crypto scheme
  to encrypt one letter into two characters.
  With only 26 different letters
  I am able to encrypt up to 26*26 different characters.
  The big problem again is sharing the key, but the cipher
  is easily broken anyway.
  The message is in the current language,
  is written with correct case and punctuation. There are no line breaks.

  Good luck!

  wxftuzjlzcmfymbjuomfymziftuzhywp hwftbj lroyrlzcttypymoylr ymjfzihy
  zfoyhyhymfjloy hybjrlrloyhyhyuhbjuouottwp mbmfhy uzftym ymftft
  lrziuhuhzirlbjuoym oyziymjfoyzchp nimfhy ziymhc mboyuouohp jlftftlr
  dzftvywp mduzymoyzc ymjfzihy udoyttniftzclr mfhy hyftuobjymziftuzxu
  ftuhuhuhrlzfhyhyjlftjfypwp
  """
  Then I analyze the first word
  And I noticed that it has a size of 32 characters
  And I divided 32 into 2 and discovered that it is a 16 letter word
  And I followed the context of the challenge
  And I assumed it was a congratulation
  And I discovered that the word "congratulations" was adapted to encryption

Scenario: Successful solution
  Given the challenge url
  Then I open it with Google Chrome
  And I compared the letters of the word i knew
  And I formed words with the help of my common sense
  Then I discovered a text
  """
  congratulations. you decrypted this message successfully. was not too
  difficult either was its well good job. enter this keyword as
  solution offfcmssgohp.
  """
  Then I put as answer offfcmssgohp and the answer is correct.
  Then I solved the challenge.
