## Version 1.0
## language: en

Feature: 1-cryptography-listbrain
   Site:
    listbrain.awardspace.biz
  Category:
    cryptography
  User:
    lekriss1234
  Goal:
    Discover the decrypted text

    Background:
  Hacker's software:
    |<software>            |<version>       |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  Machine information:
    Given the challenge URL
    """
    http://listbrain.awardspace.biz/?p=CR001
    """
    Then I open the URL with Mozilla
    And I see the crypted text
    """
    aurQn4xNtr
    """

  Scenario: Success: using web tool for caesar cipher
  in Ascii mode
    Given I opened the following URL
    """
    https://www.geocachingtoolbox.com/index.php?lang=es&page=caesarCipher
    """
    Then I put the cypted text in the tool
    And I start thinking about the given values
    Then I set the ROT as 13
    And I obtain the following text
    """
    TheDa'kAge
    """
    And i think that is not natural
    Then I ommit certain characters from the alphabet
    And I obtain the following text
    """
    TheDarkAge
    """
    Then I put the text in the designated space
    And submit it
    Then the password is accepted
    Then I conclude that ROT 13 in Ascii code was correct
    And I capture the flag
    And I solve the challenge
