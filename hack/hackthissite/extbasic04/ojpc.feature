# language: en

Feature: Solve the challenge 4
  From the hacksithissite.org website
  From the extbasic category
  With my username siegfrieg94

  Background:
    Given a program in an unknown language 
    And I have to decipher what will be the output of this program

  Scenario: Successful Solution
    When I analyze the program
    Then I realize that there are two variables defined by user
    Then I find that output have the variables values
    Then I get the answer
    And I solve the challenge

