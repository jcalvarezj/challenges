## Version 2.0
## language: en

Feature: Referer - miscellaneous - rankk
  Site:
    https://www.rankk.org/challenges/referer.py
  User:
    JuanMusic1 (wechall)
  Goal:
    Find the flag in the page

  Background:
  Hacker's software:
    | Ubuntu      | 18.04.3 |
    | Firefox     | 72.0.1  |
    | Burp Suite  | 2.1.07  |
  Machine information:
    Given A link in the challenge page
    When I access it
    Then I see the next string
    """
      The solution is the referer string below:
      http://127.0.0.1/juanmusic1.html
    """

  Scenario: Fail: Searching in the HTML
    Given The page with the message
    When I sarch for the flag in the HTML
    Then The page doesn't contains it

  Scenario: Success: Modifying the Referer header
    Given The page with the message
    When I capture the request with burp suite
    Then I see the Referer header with other link
    And I change this link with link of the message
    When I submit this new request
    Then I solve the challenge
