## Version 2.0
## language: en

Feature: Cracking-Root Me
  Site:
    www.root-me.org
  Category:
    Cracking
  User:
    ciberstein
  Goal:
    Find the password disassembling the .bin file

  Background:
  Hacker's software:
    | <Software name> |    <Version>     |
    | Windows         | 10.0.17134 (x64) |
    | Chrome          | 70.0.3538.77     |
    | MinGW           | 0.602.22         |
  Machine information:
    Given I am accessing the challenge site from my browser
    And the statement is displayed
    """
    ELF - Ptrace

    Start the challenge
    """
    Then I selected the challenge button to start
    And I download the .bin file

  Scenario: Success:Debugging the application
    Given I have MinGW on my system
    And I using GDB to debug the app
    Then I loaded the binary in gdb and looked for the debugger detection
    """
    Breakpoint 1, 0x080483fe in main ()
    (gdb) x/20i $eip
    0x80483fe <main+14>:    sub    esp,0x14
    0x8048401 <main+17>:    mov    DWORD PTR [ebp-12],0x80c2888
    0x8048408 <main+24>:    push   0x0
    0x804840a <main+26>:    push   0x1
    0x804840c <main+28>:    push   0x0
    0x804840e <main+30>:    push   0x0
    0x8048410 <main+32>:    call   0x8058a70 <ptrace>  ;
    should be this one, I guess...
    0x8048415 <main+37>:    add    esp,0x10
    0x8048418 <main+40>:    test   eax,eax                      ;
    promising test
    0x804841a <main+42>:    jns    0x8048436 <main+70>
    0x804841c <main+44>:    sub    esp,0xc
    0x804841f <main+47>:    push   0x80c2894             ;
    "Debugger detecté ... Exit"  this is it
    0x8048424 <main+52>:    call   0x80492d0 <puts>
    0x8048429 <main+57>:    add    esp,0x10
    0x804842c <main+60>:    mov    eax,0x1
    0x8048431 <main+65>:    jmp    0x80484f9 <main+265>
    0x8048436 <main+70>:    sub    esp,0xc
    0x8048439 <main+73>:    push   0x80c28b0
    0x804843e <main+78>:    call   0x80492d0 <puts>
    0x8048443 <main+83>:    add    esp,0x10
    """
    Then I patched the binary to jump over the test
    """
    8048408:       6a 00                   push   0x0
    804840a:       6a 01                   push   0x1
    804840c:       6a 00                   push   0x0
    804840e:       6a 00                   push   0x0
    8048410:       e8 5b 06 01 00          call   8058a70 <ptrace>
    8048415:       83 c4 10                add    esp,0x10
    8048418:       85 c0                   test   eax,eax
    804841a:       79 1a                   jns    8048436 <main+0x46>
    804841c:       83 ec 0c                sub    esp,0xc
    804841f:       68 94 28 0c 08          push   0x80c2894
    8048424:       e8 a7 0e 00 00          call   80492d0 <_IO_puts>
    8048429:       83 c4 10                add    esp,0x10
    804842c:       b8 01 00 00 00          mov    eax,0x1
    8048431:       e9 c3 00 00 00          jmp    80484f9 <_notng+0x62>
    8048436:       83 ec 0c                sub    esp,0xc

    =>

    8048408:       e9 29 00 00 00          jmp    8048436 <main+0x46>
    804840d:       90                      nop
    804840e:       6a 00                   push   0x0
    8048410:       e8 5b 06 01 00          call   8058a70 <ptrace>
    8048415:       83 c4 10                add    esp,0x10
    8048418:       85 c0                   test   eax,eax
    804841a:       79 1a                   jns    8048436 <main+0x46>
    804841c:       83 ec 0c                sub    esp,0xc
    804841f:       68 94 28 0c 08          push   0x80c2894
    8048424:       e8 a7 0e 00 00          call   80492d0 <_IO_puts>
    8048429:       83 c4 10                add    esp,0x10
    804842c:       b8 01 00 00 00          mov    eax,0x1
    8048431:       e9 c3 00 00 00          jmp    80484f9 <_notng+0x62>
    8048436:       83 ec 0c                sub    esp,0xc
    """
    And I then looked for my password reading
    And I found an interesting jump through eax:
    """
    0x8048486 <main+150>:   call   0x8048f90 <fgets>
    0x804848b <main+155>:   add    esp,0x10
    0x804848e <main+158>:   lea    eax,ds:0x8048497
    0x8048494 <main+164>:   inc    eax
    0x8048495 <main+165>:   jmp    eax                      ;
    where are we jumping here ?
    0x8048497 <main+167>:   mov    eax,0x8bea558a
    0x804849c <main+172>:   inc    ebp
    0x804849d <main+173>:   hlt
    """
    Then I stepped in and examined the code:
    """
    0x8048498 <main+168>:   mov    dl,BYTE PTR [ebp-22]     ;
    pointing to the password I entered
    0x804849b <main+171>:   mov    eax,DWORD PTR [ebp-12]   ;
    pointing to "ksuiealohgy"
    0x804849e <main+174>:   add    eax,0x4                  ;
    5th char -> 'e'
    0x80484a1 <main+177>:   mov    al,BYTE PTR [eax]
    0x80484a3 <main+179>:   cmp    dl,al                    ;
    char comparison
    0x80484a5 <main+181>:   jne    0x80484e4 <main+244>
    0x80484a7 <main+183>:   mov    dl,BYTE PTR [ebp-21]
    0x80484aa <main+186>:   mov    eax,DWORD PTR [ebp-12]
    0x80484ad <main+189>:   add    eax,0x5                  ;
    6th char -> 'a'
    0x80484b0 <main+192>:   mov    al,BYTE PTR [eax]
    0x80484b2 <main+194>:   cmp    dl,al
    0x80484b4 <main+196>:   jne    0x80484e4 <main+244>  -> wrong password
    0x80484b6 <main+198>:   mov    dl,BYTE PTR [ebp-20]
    0x80484b9 <main+201>:   mov    eax,DWORD PTR [ebp-12]
    0x80484bc <main+204>:   inc    eax
    0x80484bd <main+205>:   mov    al,BYTE PTR [eax]                    ;
    2nd char -> 's'
    0x80484bf <main+207>:   cmp    dl,al
    0x80484c1 <main+209>:   jne    0x80484e4 <main+244>   -> wrong password
    0x80484c3 <main+211>:   mov    dl,BYTE PTR [ebp-19]
    0x80484c6 <main+214>:   mov    eax,DWORD PTR [ebp-12]
    0x80484c9 <main+217>:   add    eax,0xa                  ; 11th char -> 'y'
    0x80484cc <main+220>:   mov    al,BYTE PTR [eax]
    0x80484ce <main+222>:   cmp    dl,al
    0x80484d0 <main+224>:   jne    0x80484e4 <main+244>     ;
    wrong password
    0x80484d2 <main+226>:   sub    esp,0xc
    0x80484d5 <main+229>:   push   0x80c297a
    0x80484da <main+234>:   call   0x80492d0 <puts>         ;
    good password : "easy"
    0x80484df <main+239>:   add    esp,0x10
    0x80484e2 <main+242>:   jmp    0x80484f4 <main+260>
    0x80484e4 <main+244>:   sub    esp,0xc
    """
    And I saw le following line
    """
    good password : "easy"
    """
    Then I put as answer "easy" and the answer is correct.
    Then I solved the challenge.
