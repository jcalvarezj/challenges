## Version 2.0
## language: en

Feature: Web Server-Root Me
  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    Seruex
  Goal:
    find the password using weak password

  Background:
  Hacker's software:
    | <Software name> |  <Version>     |
    | Parrot Os       |  3.9 (64 bits) |
    | {Firefox        | 56.4 (64 bits) |
  Machine information:
    Given I the challenge url
    #https://www.root-me.org/es/Challenges/Web-Servidor/Weak-password
    Then I click on start challenge
    And enter to popup login

  Scenario: Fail: login whit randon user and password
    Given I access to popup login to resolve challenge
    And I put default random user and password
    Then I put on admin field (admin)
    And I put on password field (123)
    But the pop up window reappears
    Then I put on admin field (admin)
    And I put on password field (12345)
    But the pop up window reappears
    Then I conclude that I can't use this credential to access page
    And the credentials (admin, 123) and (admin, 12345) did not work

  Scenario: Success : login whit admin user and admin password
    Given I access to popup login to resolve challenge
    Then I put on admin field (admin)
    And I put on password field (admin)
    Then I see the message "Well done, you can use this password to validate"
    #the challenge
    And I conclude that credential to access page are user:admin, password:admin
    And solved the challenge
