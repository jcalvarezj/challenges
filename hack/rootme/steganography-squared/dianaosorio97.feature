## Version 2.0
## language: en

Feature: Steganography-rootme
  Site:
    rootme
  Category:
    Steganography
  User:
    dianaosorio97
  Goal:
    Get the message

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
    | Mozilla Firefox | 65.0.1-2    |
  Machine information:
    Given I am accessing the following url
    """
    https://www.root-me.org/es/Challenges/Esteganografia/Squared
    """
    And I click in the button start
    And I am redirect to the following link
    """
    http://challenge01.root-me.org/steganographie/ch2/ch2.jpg
    """
    And I see the image
    And I download it

  Scenario: Success: Using command string
    Given I download the image
    And I use the terminal to write the following command
    """
    string bla.jpg
    """
    And I see the message
    """
    HAHAHA
    """
    Then I put the password in the answer
    Then I solve the challenge

