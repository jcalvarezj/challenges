#!/usr/bin/env python
# Python emdeefive.py
"""
This script returns publication data creates a session that causes the text to
be divided into hash with hashlib. When you make a request, a new connection is
made each time, so that you make a request for obtaining and publishing with
a single connection.
"""

import hashlib
import requests


URI = "http://docker.hackthebox.eu:41338"
PROXIES = {}  # {'http':'http://127.0.0.1:8080'}


def get_and_hash(ret):

    """
    This function is used to decrypt MD5
    """
    begin = ret.find("<h3 align='center'>") + 19
    end = ret.find("</h3>")
    md5_string = ret[begin:end].encode('utf-8')
    digest = hashlib.md5(md5_string).hexdigest()
    return digest


SESSION = requests.session()
REQ = SESSION.GET(URI, proxies=PROXIES)
MD5 = get_and_hash(REQ.text)
REQ = SESSION.POST(URI, data={"hash": MD5}, proxies=PROXIES)
print REQ.text
