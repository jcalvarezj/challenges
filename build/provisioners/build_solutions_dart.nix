let
  pkgs = import ../pkgs/stable.nix;
  inputs = [];
in
  import ../dependencies/build_solutions.nix { inherit pkgs; inherit inputs; }
