;; $ clj-kondo --lint slayfer1112.clj
;; linting took 16ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [datv (str/split-lines (slurp *in*))
        head (str/split (datv 0) #" ")
        body (subvec datv 1 (alength (to-array-2d datv)))]
    [head
     body]))

(defn savings [s r y p]
  (if (<= s r)
    (savings (read-string (format "%.2f" (* s p))) r (inc y) p)
    y))

(defn solution [data]
  (let [s (double (data 0))
        r (double (data 1))
        p (double (+ (/ (data 2) 100) 1))
        y 0]
    (print (str (savings s r y p) " "))))

(defn main []
  (let [[_ body] (get-data)]
    (doseq [x body]
      (solution (vec (map read-string (str/split x #" ")))))
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 109 109 273 302 163 137 163 21 92 324 163 163 221 163 7
