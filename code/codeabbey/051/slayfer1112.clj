;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 0 (alength (to-array-2d data)))]
    [head
     body]))

(defn abs [n] (max n (- n)))

(defn solution [data]
  (let [dices [1.5 2.5 3.5 4.5 5.5 6.5]
        values (str/split data #" ")
        dat (subvec (vec (map #(read-string %) values)) 0 100)
        mini (reduce min dat)
        maxi (reduce max dat)
        pro (double (/ (reduce + dat) 100))
        prob (vec (map #(Math/round (double (abs (- (* % mini) pro)))) dices))
        faces (* 2 (+ (.indexOf prob (reduce min prob)) 1))]
    (if (<= (* mini faces) maxi)
      (print (str mini "d" faces " "))
      (print (str mini "d" (- faces 2) " ")))))

(defn main []
  (let [[_ body] (get-data)]
    (doseq [x body]
      (solution x))
    (println)))
(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 0 0 1 1 0 1 1 1 1 1 1 0 0 0 0 1 1 1 1 0
