#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.79 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

def sd(list : Array(Number)) : Float64
  mean = list.sum / list.size
  s_x_u_2 = list.map { |x| (x - mean) ** 2} .sum
  Math.sqrt(s_x_u_2 / (list.size - 1))
end

def sd_dice(n, sides) : Float64
  Math.sqrt(n * (sides ** 2 - 1) / 12)
end

DICE = {2, 4, 6, 8, 10, 12}

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split("\n") if fluid
args = args[0].split("\n")  if !fluid

args = args.map { |x| x[...-1].split.map { |y| y.to_i} }
args.delete [] of Int32

standard_d = [] of Tuple(Int32, Int32, Float64) # {#dice, #sides, sd}

DICE.each do |r|
  (1..5).each do |n|
    standard_d << {n, r, sd_dice(n, r)}
  end
end

args.each do |x|
  s_d = sd(x)
  diff = (standard_d[0][2] - s_d).abs

  n_dice = standard_d[0][0]
  n_sides = standard_d[0][1]

  standard_d.reject {|d| d[0] > x.min|| d[0] * d[1] < x.max}
  .each do |s_deviation|
    if (s_deviation[2] - s_d).abs < diff
      diff = (s_deviation[2] - s_d).abs
      n_dice = s_deviation[0]
      n_sides = s_deviation[1]
    end
  end

  print "#{n_dice}d#{n_sides} "
end

puts

# $ ./richardalmanza.cr
# 2d10 5d6 2d6
