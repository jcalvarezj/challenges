# !/usr/bin/crystal

# $ ameba cleancamera.cr
# Inspecting 1 file.


def convert_data_int()
  data_array = File.read("DATA.lst")
  convert_data = [] of Array(Int32)
  data_array.each_line do |x|
    data_int = [] of Int32
      (x.split).each do |j|
        j = j.to_i
        data_int << j
    end
    convert_data << data_int
  end
  convert_data[1..]
end

def min_function(array)
  min_array = [] of Int32
  if array[0] < array[1]
    min_array << array[0]
  elsif array[1] < array[0]
    min_array << array[1]
  end
  puts min_array
end


data_fun = convert_data_int()
data_fun.each do |x|
  min_function(x)
end

# $ crystal cleancamera.cr
# 586254 6209071 -6824287 -7578938 2536687 -6785022 -8660201 1462649 -1406074
# -6803702 -3391748 1668688 -832977 -9849648 -7385857 -7910251 -9550187
# -1052633 -4872152 -8570407 -1474502
