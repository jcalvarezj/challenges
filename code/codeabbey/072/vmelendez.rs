/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

fn method(a: usize, xcur: usize, c: usize, m: usize) -> usize {
  let xnext = (a * xcur + c) % m;
  return xnext;
}

fn test_while(i: usize, mut random: usize, nms: &[i32]) {
  let cons = "bcdfghjklmnprstvwxz";
  let vows = "aeiou";
  for j in 0..nms[i] {
    let idx = j + 1;
    if idx % 2 == 0 {
      print!("{}", vows.as_bytes()[random % 5] as char);
    } else {
      print!("{}", cons.as_bytes()[random % 19] as char);
    }
    random = method(445, random, 700001, 2097152);
  }
}

fn main() -> std::io::Result<()> {
  let input: Vec<i32> = {
    let mut a = String::new();
    std::io::stdin().read_line(&mut a).unwrap();
    a.split_whitespace()
      .map(|x| x.trim().parse::<i32>().unwrap())
      .collect()
  };

  let nums: Vec<i32> = {
    let mut n = String::new();
    std::io::stdin().read_line(&mut n).unwrap();
    n.split_whitespace()
      .map(|x| x.trim().parse::<i32>().unwrap())
      .collect()
  };

  let random: usize = method(445, input[1] as usize, 700001, 2097152);
  let mut i = 0;
  while i < nums.len() {
    test_while(i, random, &nums);
    print!(" ");
    i = i + 1;
  }
  println!();
  Ok(())
}

/*
  $ cat DATA.lst | ./vmelendez
  ciz pesigemi kobex pate hedop cadehaco vajus madew honi dire mop jisa
  fuha wim nakuco jumikuse zodadiz hetacudo biminez sib
*/
