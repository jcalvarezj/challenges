#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 77.13 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Int32)
  data.each_line do |x|
    inter = [] of Int32
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_i : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def rounding(array)
  a = array[0]
  b = array[1]
  c = array[2]

  if a<b && a<c
    print "#{a} "
  elsif b<a && b<c
    print "#{b} "
  else
    print "#{c} "
  end

end

data = data_entry()
data.each do |x|
  rounding(x)
end
puts

# $ ./slayfer1112
# -6428612 -6763437 -8697062 -6773166 1379899 -9853923 -9051368 -9889194
# -3497897 -109995 -6259293 -2687905 2260302 -9227149 -5619399 -5473322
# -4524690 1006052 -7347072 -5923032 -2040122 -5878363 -8413666 -5079171
# 6506224 -7513502 -3398672
