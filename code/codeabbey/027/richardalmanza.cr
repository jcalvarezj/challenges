#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 4.49 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

swap_counter = 0
pass_counter = 1

(0...args.size - 1).each  do |x|
  flag = true
  (0...args.size - 1 - x).each do |y|
    if args[y] > args[y + 1]
      swap_counter += 1
      temp = args[y]
      args[y] = args[y + 1]
      args[y + 1] = temp
      if flag
        pass_counter += 1
        flag = false
      end
    end
  end
end

puts "#{pass_counter} #{swap_counter}"
# $ ./richardalmanza.cr
# 16 97
