#!/bin/bash
:<<-!
 # sudo apt-get install bc
 $ shellcheck congracift.sh
 $
!
flag=true
consDivi=0.555555556    #this is equal a 5/9
consRes=32
while read -r line; #read line on line
  do
    IFS=', ' read -r -a array <<< "$line" #split
    for element in "${array[@]}"
      do
        if [ $flag = false ] ; then
          echo "($element-$consRes)*$consDivi" | bc | awk '{printf "%.f ", $1}'
        fi
    flag=false
      done
done < DATA.lst

:<<-!
 $ bash congracift.sh
 $ 0 247 116 26 42 38 146 57 139 77 94 117 309 232 264 11 138 169 278 132 126
   263 293 138 262 123 170 298 154 11 121 154 257 237
!
