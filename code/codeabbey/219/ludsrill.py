# $ prospector ludsrill.py
# Check Information
# =================
#         Started: 2020-08-21 10:10:47.965164
#         Finished: 2020-08-21 10:10:49.953624
#       Time Taken: 1.99 seconds
#        Formatter: grouped
#         Profiles: default, no_doc_warnings, no_test_warnings,
#                   strictness_medium, strictness_high,
#                   strictness_veryhigh, no_member_warnings
#       Strictness: None
#   Libraries Used:
#        Tools Run: dodgy, mccabe, pep8, profile-validator, pyflakes, pylint
#   Messages Found: 0


import math
from typing import List, Dict, Tuple


def getdata() -> Tuple[List[List[float]], Dict[str, List[List[float]]]]:
    dataraw = open('DATA.lst', 'r')
    topology: List[int] = []
    data: List[float] = []
    x_inputs: List[List[float]] = []
    weight_dictionary: Dict[str, List[List[float]]] = {}
    data = []
    con = ""

    for i in dataraw.readline():
        if i not in (" ", "\n"):
            topology.append(int(i))

    for i in dataraw:
        for j in i:
            if j not in (" ", "\n"):
                con += j
            else:
                data.append(float(con))
                con = ""

    dataraw.close()
    x_inputs, weight_dictionary = sortdata(topology, data)
    return x_inputs, weight_dictionary


def sortdata(topology: List[int], data: List[float]
             ) -> Tuple[List[List[float]], Dict[str, List[List[float]]]]:
    count, count3, count4 = 0, -1, 0
    x_inputs: List[List[float]] = []
    general_weight: List[List[float]] = []
    weight_dictionary: Dict[str, List[List[float]]] = {}
    length = len(topology)
    for _ in range(1, length):
        count4 += 1
        for i in range(0, topology.__getitem__(count4)):
            general_weight.append([])
            count3 += 1
            for _ in range(0, topology[count4 - 1]):
                general_weight[count3].append(data[count])
                count += 1

    inputs = int(data[count])
    count += 1
    for i in range(0, inputs):
        x_inputs.append([])
        for _ in range(0, topology[0]):
            x_inputs[i].append(data[count])
            count += 1
    weight_dictionary = dictionary(topology, general_weight)
    return x_inputs, weight_dictionary


def dictionary(topology: List[int], general_weight: List[List[float]]
               ) -> Dict[str, List[List[float]]]:
    count2: int = 0
    weight_dictionary: Dict[str, List[List[float]]] = {}
    length: int = len(topology)
    for i in range(0, length):
        temp: List[List[float]] = []
        for _ in range(0, topology[i]):
            if i > 0:
                temp.append(general_weight[count2])
                weight_dictionary["w" + str(i)] = temp
                count2 += 1

    return weight_dictionary


def sigmoid(sum_product_wx: List[List[float]]) -> List[List[float]]:
    k: List[List[float]] = []
    for i in range(0, sum_product_wx.__len__()):
        k.append([])
        for j in range(0, sum_product_wx[0].__len__()):
            k[i].append(2 / (1 + math.exp(-sum_product_wx[i][j] * 2)) - 1)
    return k


def product(general_weight: List[List[float]], x_inputs: List[List[float]]
            ) -> List[List[float]]:
    sum_product_wx: List[List[float]] = []
    length = int(len(general_weight))
    for i in range(0, length):
        sum_product_wx.append([])
        for k in range(0, x_inputs[0].__len__()):
            suma: float = 0
            for j in range(0, general_weight[0].__len__()):
                suma = suma + general_weight[i][j] * x_inputs[j][k]
            sum_product_wx[i].append(suma)
    return sum_product_wx


def forward_propagation(x_inputs: List[List[float]],
                        weight_dictionary: Dict[str, List[List[float]]]
                        ) -> List[float]:
    output: List[float] = []
    if len(weight_dictionary) == 3:
        sum_product_1 = product(weight_dictionary["w1"], x_inputs)
        activation_1 = sigmoid(sum_product_1)
        sum_product_2 = product(weight_dictionary["w2"], activation_1)
        activation_2 = sigmoid(sum_product_2)
        sum_product_3 = product(weight_dictionary["w3"], activation_2)
        activation_3 = sigmoid(sum_product_3)
        output = result(activation_3)

    if len(weight_dictionary) == 4:
        sum_product_1 = product(weight_dictionary["w1"], x_inputs)
        activation_1 = sigmoid(sum_product_1)
        sum_product_2 = product(weight_dictionary["w2"], activation_1)
        activation_2 = sigmoid(sum_product_2)
        sum_product_3 = product(weight_dictionary["w3"], activation_2)
        activation_3 = sigmoid(sum_product_3)
        sum_product_4 = product(weight_dictionary["w4"], activation_3)
        activation_4 = sigmoid(sum_product_4)
        output = result(activation_4)

    return output


def result(activation: List[List[float]]) -> List[float]:
    predict: List[float] = []
    length = activation.__len__()
    for i in range(0, len(activation[0])):
        for j in range(0, length):
            predict.append(activation[j][i])
    return predict


if __name__ == "__main__":
    X_SORT, D_SORT = getdata()
    X_FINAL = [[X_SORT[j][i] for j in range(len(X_SORT))]
               for i in range(len(X_SORT[0]))]

    FINAL_PREDICTION = forward_propagation(X_FINAL, D_SORT)
    CONCATENATE_OUTPUT = ""
    for i_count in FINAL_PREDICTION:
        CONCATENATE_OUTPUT += str(i_count) + " "

    print(CONCATENATE_OUTPUT)

# $ python ludsrill.py
# -0.5238501125882231 0.015063481387463717
# 0.35010031920746965 -0.5795979774898365
# -0.19156390327593453 0.08349190434416753
# -0.5528123758934743 -0.008768523348373747
# 0.3443598668033172 0.46567227657814403
# 0.36279933899483696 0.24444658929947471
# 0.33247073584264997 0.22786928576003795
# 0.1810060408637253
