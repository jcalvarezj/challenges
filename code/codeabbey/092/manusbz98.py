# mypy --strict manumeco98.py
# Success: no issues found in 1 source file

""""
Autor: Manuel Semeco

Solution coddeabbey 092 Binary Heap

This is a refactoring solution wich goes from a one based
function[the latest version before this one, commit :4a431c45]
to a new one based class solution just
for reducing complexity.
"""
from typing import List, Sequence, Union
import math


class BinaryHeap():
    """class for binary heap"""
    # this is the OUTPUT before processing: wich is the binary heap

    def __init__(self, input_a: List[int]) -> None:
        self.input_a: List[int] = input_a
    l_child: int = int()
    r_child: int = int()
    out: List[int] = list()
    current_element: int = int()
    crt_node: int = int()

    def to_list(self) -> List[int]:
        """return a list with the binary heap"""
        self._processing_input()
        return self.out

    def __repr__(self) -> str:
        self._processing_input()
        copy_out = [str(element) for element in self.out]
        return " ".join(copy_out)

    @staticmethod
    def swap(input_b: List[int], index1: int, index2: int) -> List[int]:
        """this function swaps elements in a list"""
        aux_list = input_b.copy()
        aux_list[index1], aux_list[index2] = aux_list[index2], aux_list[index1]
        return aux_list

    def _verifying_current_el(self) -> None:
        # this function verifies the current elements based on the rules of
        # making a binary heap explained on codeabey task view 092.
        if self.current_element == 0:
            # this block execute the delete comand.
            self.out = self.swap(self.out, 0, len(self.out) - 1)
            self.out.pop(len(self.out) - 1)
            self.crt_node = self.out[0]
            left_child_index = 2 * self.out.index(self.crt_node) + 1
            right_child_index = 2 * self.out.index(self.crt_node) + 2
            self.l_child = self.out[left_child_index]
            self.r_child = self.out[right_child_index]
            # the function bellow verifies rules for the new root node
            self._crt_node_verifiying_loop()
        else:
            # this block add a new node to the heap
            self.out.append(self.current_element)
            current_node_index: int = self.out.index(self.current_element)
            parent_index: int = math.floor((current_node_index - 1) * 0.5)
            parent_node: int = self.out[parent_index]
            self.crt_node = self.current_element
            while parent_node > self.current_element:
                self.out = self.swap(
                    self.out, parent_index, current_node_index)
                current_node_index = self.out.index(self.current_element)
                parent_index = math.floor((current_node_index - 1) * 0.5)
                self.crt_node = self.out[current_node_index]
                parent_node = self.out[parent_index]
                if current_node_index <= 0:
                    break

    def _crt_node_verifiying_loop(self) -> None:
        def index_error_handler() -> int:
            try:
                self.l_child = self.out[2 * self.out.index(self.crt_node) + 1]
            except IndexError:
                return 1
            try:
                self.r_child = self.out[2 * self.out.index(self.crt_node) + 2]
            except IndexError:
                return 1
            return 0

        while self.crt_node > self.l_child or self.crt_node > self.r_child:
            if self.crt_node > self.r_child and self.r_child < self.l_child:
                self.out = self.swap(self.out, self.out.index(self.r_child),
                                     self.out.index(self.crt_node))
                if index_error_handler():
                    break
            else:
                self.out = self.swap(self.out, self.out.index(self.l_child),
                                     self.out.index(self.crt_node))
                if index_error_handler():
                    break

    def _processing_input(self) -> None:
        # main private function that init all the other ones. Mutating all
        # the attributes for the process of converting a list of integers into
        # a list like binary heap.
        self.out.clear()
        for element in self.input_a:
            self.current_element = element
            if len(self.out) == 0:
                self.out.append(self.current_element)
            else:
                self._verifying_current_el()


INPUTS: Sequence[Union[str, int]] = input().split()
AUX = [int(element) for element in INPUTS]
INPUTS = AUX.copy()
CL = BinaryHeap(INPUTS)
print(CL)
# cat DATA.lst | python3 manumeco98.py
# 3 4 6 5 7 8 11 10 9
