% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0]).

-compile(andresclm).

start() ->
  Data = read_file("DATA.lst"),
  Matrix = [[ [Letter]|| Letter<-Line]|| Line<-Data],
  Results = [ get_results(Row) || Row<-Matrix ],
  [io:format("~s ~s ",Result)||Result<-Results].

get_results(List) ->
  MaxHex = lists:max(List),
  MinHex = lists:min([Hexadecimal||Hexadecimal<-List, Hexadecimal>"0"]),
  MaxHexNumber = get_max_number(List, MaxHex),
  MinHexNumber = get_min_number(List, MinHex),
  [MinHexNumber,MaxHexNumber].

get_max_number(List,MaxHex) when MaxHex=/=hd(List) ->
  get_number(List, MaxHex);
get_max_number(List, MaxHex) when MaxHex=:=hd(List) ->
  [Head|Tail] = List,
  string:join([Head,get_max_number(Tail, lists:max(Tail))],"").

get_min_number(List,MinHex) when MinHex=/=hd(List) ->
  get_number(List, MinHex);
get_min_number(List, MinHex) when MinHex=:=hd(List) ->
  [Head|Tail] = List,
  string:join([Head,get_min_number(Tail, lists:min(Tail))],"").

get_number(NumString, Hex) ->
  [Head|Tail] = NumString,
  Temp = string:reverse(string:join([Hex|Tail],"")),
  string:reverse(string:join(string:replace(Temp,Hex,Head),"")).

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  [_|Data] = string:lexemes(erlang:binary_to_list(Binary), "\n"),
  Data.

% $ erl -noshell -s andresclm -s init stop
% 16F1EB8F838DC629365CA7FA3 F6F1EB8F838DC6293651A7CA3
% 27422082B7DA66EF4A F742208227DA66EB4A
% 249D9B80A74EDB3FE48 F49D9B80A24EDB37E48
% 1FE664F58ACFE FFE164F58AC6E
% 17068A8C967C56DE77 E7068A8C967C561D77
% 184A8A32626B08B606FCF54CBBD9 F84A8A32621B08B606FC654CBBD9
% 278DB55558B8979 D785B25558B8979
% 1F38F0845400392 FF3830845400192
% 103CD8121DAB43D731A DA3CD8121DAB4317310
% 1886C86E294347F2 F886C86E29434721
% 142EEC32AA6BA E41E2C32AA6BA
% 13D309AEB32E89E99FEBEC7 F31309AEB32E89E99DEBEC7
% 2FC5D7EAA0DB979 FEC5D72AA0DB979
% 1B4352DF2BAB823740D FB4352DB2BA1823740D
% 1CC2CAD627BD208 DCC2CAD627B8201
% 1F75B3743AC7A992665203 F375B1743AC7A992665203
% 1A40C42571CDDD413ED897 EA40C42571CDDD4113D897
% 1D810948CFCCCCF FD810948CFCCC1C
% 20D5EAA0B9E495 E0D5EAA0B99425
% 1300395B54820A1122E8E EE00395B54820A1122381
