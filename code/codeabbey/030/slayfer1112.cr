#! /usr/bin/crystal

# $ ameba --all --fail-level Convention slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 8.92 milliseconds
# $ crystal build --error-trace --error-on-warnings \
# --threads 1 --no-codegen slayfer1112.cr

args = gets

def solution(array)
  text = array
  sol = ""
  i = text.size - 1
  while i >= 0
    sol += text[i]
    i -= 1
  end
  print "#{sol} "
end

if args
  args = args.split("")
  solution(args)
  puts
end

# $ cat DATA.lst | crystal slayfer1112.cr
# aococ erehw ffo yhw flehs nwolc eraf
