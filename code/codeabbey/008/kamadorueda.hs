-- $ ghc -o kedavamaru kedavamaru.hs
--   [1 of 1] Compiling Main ( kedavamaru.hs, kedavamaru.o )
--   Linking code ...
-- $ hlint kedavamaru.hs
--   No hints

module Main where

  import System.IO
  import Control.Monad

  processfile :: Handle -> IO ()
  processfile ifile =
    do
      iseof <- hIsEOF ifile
      Control.Monad.unless iseof $
        do
          line <- hGetLine ifile
          let vector_ints = map read $ words line :: [Double]
          let a = head vector_ints
          let b = vector_ints!!1
          let n = vector_ints!!2
          let r = round (n*(a + (a + (n-1)*b))/2)
          print r
          processfile ifile

  main =
    do
      ifile <- openFile "DATA.lst" ReadMode
      line <- hGetLine ifile
      processfile ifile
      hClose ifile

-- $ ./kedavamaru
--   91180
--   49039
--   29762
--   9860
--   3552
--   867
--   97119
--   3225
--   779
--   3654
