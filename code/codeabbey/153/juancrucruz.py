#!/usr/bin/python3
'''
$ pylint juancrucruz.py #linting

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

$
'''
# This code requires to have installed the
# gmpy2 library, because i had to work with high
# precision integers
# pylint: disable=I0023, E0401

from math import floor, ceil
from typing import Tuple, Any
import sys
import gmpy2  # type: ignore


def fermat(n_value: int) -> Tuple[int, Any]:
    """Calculates factors p and q"""
    # I implemented just the basic method described here:
    # https://en.wikipedia.org/wiki/Fermat%27s_factorization_method
    n_value = gmpy2.mpz(n_value)
    gmpy2.get_context().precision = 1024
    a_value = ceil(gmpy2.sqrt(n_value))
    while True:
        b_value = a_value * a_value - n_value
        if gmpy2.sqrt(b_value) == floor(gmpy2.sqrt(b_value)):
            break
        a_value += 1
    p_value = a_value + gmpy2.sqrt(b_value)
    q_value = a_value - gmpy2.sqrt(b_value)
    return p_value, q_value


def exteuclidian(x_value: int, y_value: int) -> int:
    """Calculates Extended Euclidian Algorithm to find d"""
    sprev = 1
    scur = 0
    phi_n = y_value
    tprev = 0
    tcur = 1
    residue = x_value % y_value
    while residue != 0:
        q_value = x_value // y_value
        residue = x_value % y_value
        snext = sprev - q_value * scur
        tnext = tprev - q_value * tcur
        x_value = y_value
        y_value = residue
        sprev = scur
        tprev = tcur
        scur = snext
        tcur = tnext
        residue = x_value % y_value
    a_final = scur
    if a_final < 0:
        a_final = a_final % phi_n
    d_value = int(a_final)
    return d_value


def get_vars() -> Tuple[int, int]:
    """Get n and c from file"""
    if len(sys.argv) > 1:
        file = open(sys.argv[1], "r")
        lines = file.readlines()
        file.close()
        n_value = int(lines[0])
        c_value = int(lines[1])
    else:
        print("Usage: ./juancrucruz.py DATA.lst")
        sys.exit()
    return n_value, c_value


def decryptor(c_value: int, d_value: int, n_value: int) -> None:
    """Decryption function"""
    m_int = pow(c_value, d_value, n_value)
    m_str = str(m_int)
    msg = ''
    i = 0
    while i < len(m_str):
        if m_str[i:i + 2] == "00":
            break
        msg += chr(int(m_str[i:i + 2]))
        i += 2
    print(msg)


def main() -> None:
    """Fermat used to hack weak RSA (codeabbey 153)"""
    e_value = 65537
    n_value, c_value = get_vars()
    p_value, q_value = fermat(n_value)
    phi_n = (p_value - 1) * (q_value - 1)
    d_value = exteuclidian(e_value, phi_n)
    decryptor(c_value, d_value, n_value)


main()
#
#
# $ ./juancrucruz.py DATA.lst
#
# EGG AXE RAT HIP DOG LOW GEM BAR
#
