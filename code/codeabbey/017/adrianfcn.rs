/*
$ rustfmt adrianfcn.rs
$ rustc --test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn calculated_checksum(arr_checksum: &Vec<u128>) -> u128 {
  let mut result: u128 = 0;
  let seed = 113;
  let modulo = 10000007;
  for i in arr_checksum {
    result = (result + i) * seed;
    if result > 10000007 {
      result = result % modulo;
    }
  }
  result
}

fn main() -> std::io::Result<()> {
  let mut file = File::open("./DATA.lst")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;
  let mut lines = contents.lines();
  let mut arr_checksum = Vec::<u128>::new();
  lines.next();
  for line in lines {
    for c in line.split_whitespace() {
      arr_checksum.push(c.parse::<u128>().unwrap());
    }
    println!("{}", calculated_checksum(&arr_checksum));
  }
  Ok(())
}
/*
  $ ./adrianfcn
  6617103
*/
