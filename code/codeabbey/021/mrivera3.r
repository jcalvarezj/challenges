# $ lintr::lint('mrivera3.r')


counter <- function() {
  input <- scan("DATA.lst")
  data <- input[3:length(input)]
  values <- table(data)
  results <- c()
  for (i in seq_len(length(values))) {
    results <- c(results, values[[i]])
  }
  return(results)
}
counter()

# $ Rscript mrivera3.r
#  21 24 32 24 23 25 29 27 25 28 16 19 30
