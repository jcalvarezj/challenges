/*
$ cargo clippy
Checking code_115 v0.1.0 (/.../code_115)
Finished dev [unoptimized + debuginfo] target(s) in 0.13s
$ cargo build
Compiling code_115 v0.1.0 (/.../code_115)
Finished dev [unoptimized + debuginfo] target(s) in 0.62s
$ rustc sullenestd1.rs
*/

use std::io::{self, BufRead};

fn function(x: &mut Vec<f64>, aij: Vec<Vec<i64>>, mut bi: Vec<i64>) -> f64 {
  let mut fxn = 0.0;
  let mut result = 0.0;
  bi.reverse();
  for i in aij {
    for j in 0..i.len() {
      fxn += x[j] * i[j] as f64;
    }
    fxn -= bi.pop().unwrap() as f64;
    result += fxn * fxn;
    fxn = 0.0;
  }
  result
}

fn gradient(
  x: &mut Vec<f64>,
  dx: f64,
  aij: Vec<Vec<i64>>,
  bi: Vec<i64>,
) -> Vec<f64> {
  let mut g: Vec<f64> = Vec::new();
  let y = function(x, aij.clone(), bi.clone());
  let mut yn: f64;
  for i in 0..x.len() {
    x[i] += dx;
    yn = function(x, aij.clone(), bi.clone());
    x[i] -= dx;
    g.push((yn - y) / dx)
  }
  g
}

fn gradient_descent(aij: Vec<Vec<i64>>, bi: &mut Vec<i64>) {
  let mut x: Vec<f64> = vec![0.0; bi.len()];
  let mut step = 0.01;
  let mut _iter: i64 = 0;
  let mut y: f64;
  loop {
    y = function(&mut x, aij.clone(), bi.clone());
    if y < 0.0001 {
      break;
    }
    let dx = step / 10.0;
    let g = gradient(&mut x, dx, aij.clone(), bi.clone());

    let mut x_new = x.clone();
    for i in 0..x_new.len() {
      x_new[i] -= g[i] * step;
    }
    let y_new = function(&mut x_new, aij.clone(), bi.clone());
    if y_new < y {
      x = x_new;
      step = 0.1_f64.min(step * 1.25);
    } else {
      step /= 1.25;
    }
    _iter += 1;
  }
  println!("{}", _iter)
}

fn process_line(
  n: &mut i64,
  size: &mut i64,
  num: Vec<i64>,
  aij: &mut Vec<Vec<i64>>,
  bi: &mut Vec<i64>,
) {
  if *n == -1 {
    *n = num[0];
  } else if num.len() == 1 {
    *size = num[0];
  } else if *size != 0 {
    aij.push(num);
    *size -= 1;
  } else if *size == 0 {
    *bi = num;
    gradient_descent(aij.clone(), bi);
    aij.clear();
    bi.clear();
    *n -= 1;
  }
}

fn main() {
  let input = io::stdin();
  let lines = input.lock().lines();
  let mut n = -1;
  let mut size: i64 = 0;
  let mut aij: Vec<Vec<i64>> = Vec::new();
  let mut bi: Vec<i64> = Vec::new();
  for w in lines {
    let line = w.unwrap();
    let num: Vec<i64> = line
      .trim()
      .split(' ')
      .map(|w| w.parse().expect("Not a float!"))
      .collect();
    process_line(&mut n, &mut size, num, &mut aij, &mut bi);
    if n == 0 {
      break;
    }
  }
}

/*
$ cat DATA.lst | ./sullenestd1
353
285
210
60
*/
