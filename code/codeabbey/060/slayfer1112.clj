;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(def ar (atom []))

(defn sweet-harvest [isles counter limit res arr]
  (let [candies (+ res (isles counter))]
    (if (< counter (- limit 2))
      (do (sweet-harvest isles (+ 2 counter) limit candies arr)
          (if (< counter (- limit 3))
            (sweet-harvest isles (+ 3 counter) limit candies arr)
            ()))
      (if (< counter limit)
        (swap! ar conj candies)
        ()))))

(defn solution [isles]
  (let [isles-group (vec (map read-string (str/split isles #" ")))]
    (sweet-harvest isles-group 0 (count isles-group) 0 [0])
    (print (str (reduce max @ar) " "))
    (reset! ar [])))

(defn main []
  (let [[_ body] (get-data)]
    (doseq [x body]
      (solution x))
    (println)))
(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 209 177 137 201 154 129 157 162 203 162 188
;; 161 201 151 145 205 165 186 130 114 172 155
