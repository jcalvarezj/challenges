/*
$ gofmt -w neds.go
$ go vet neds.go
vet.exe: errors: 0; warnings: 0
$ go build -work neds.go
WORK=C:\Users\NEDS\AppData\Local\Temp\go-build507411318
*/

package main

import (
  "bufio"
  "fmt"
  "log"
  "os"
  "strconv"
  "strings"
)

func convString(el string) int {
  number, e := strconv.Atoi(el)
  if e != nil {
    fmt.Println(e)
  }
  return number
}

func readData() (int, []int) {
  var aux []string
  var data []int
  file, err := os.Open("DATA.lst")
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()

  lines := bufio.NewScanner(file)
  for lines.Scan() {
    aux = append(aux, lines.Text())
  }

  if err := lines.Err(); err != nil {
    log.Fatal(err)
  }

  for i := 1; i < len(aux); i++ {
    element := strings.Split(aux[i], " ")
    for j := 0; j < len(element); j++ {
      data = append(data, convString(element[j]))
    }
  }
  n := len(data)
  return n, data
}

func countGems(n int, t int) string {
  return strconv.Itoa(n*(t-1)) + " "
}

func main() {
  var out string
  n, data := readData()
  for i := 0; i < n; i += 2 {
    out += countGems(data[i], data[i+1])
  }
  fmt.Println(out)
}

/*
$ neds.exe
20 51 27 102 32 57 15 11 17
*/
