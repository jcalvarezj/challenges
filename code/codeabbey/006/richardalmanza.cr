#! /usr/bin/crystal
# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.55 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

fluid_code = true

oput = ""

if fluid_code
  args = File.read("DATA.lst").split
else
  args = ARGV.map { |x| x.split }.flatten
end

args = args[1..].map { |x| x.to_i }

(0...args.size).step(2) do |x|
  oput = "#{oput} #{(args[x] / args[x + 1]).round.to_i}"
end

puts oput

# $ ./richardalmanza.cr --
# 2 5 4 91528 8 -4 18117 30841 -1 -19 6 -90 10 10 13 4 7
