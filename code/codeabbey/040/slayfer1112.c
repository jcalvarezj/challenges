/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_SIZE 255

static int move_is_possible (int neigh[],
  int x, char m[][MAX_SIZE],
  int prevI, int prevJ)
/*@*/ {
  int y = ((int) strlen(m[0]));
  char state = m[neigh[0]][neigh[1]];
  if (neigh[0] >= 0 && neigh[0] < x && neigh[1] >= 0 && neigh[1] < y) {
    if (state == '1' && (neigh[0] != prevI || neigh[1] != prevJ))
    { return 1; }
    else { return 0; }
  }
  else { return 0; }
}

static int count_paths (int posI, int posJ, int x, char path[],
  char m[][MAX_SIZE], int prevI, int prevJ)
/*@requires maxRead(path) <= 253@*/
/*@*/ {
  int posA[2], posB[2], temp = 0;
  char n_path[MAX_SIZE];
  strcpy(n_path, path);
  posA[0] = posI, posA[1] = posJ + 1;
  posB[0] = posI + 1, posB[1] = posJ;
  /*eval if move to right is possible*/
  if (move_is_possible(posA, x, m, prevI, prevJ) == 1) {
    strcat(n_path, "R");
    temp += count_paths(posA[0], posA[1], x, n_path, m, posI, posJ);
    strcpy(n_path, path);
  /*eval if move to down is possible*/
  } if (move_is_possible(posB, x, m, prevI, prevJ) == 1) {
    strcat(n_path, "D");
    temp += count_paths(posB[0], posB[1], x, n_path, m, posI, posJ);
    strcpy(n_path, path);
  /*eval if reach the target*/
  } if (posI == (x-1) && posJ == ((int) (strlen(m[0])-1))) {return 1;}
  return temp;
}

int main (void) {
  int x, y, scan, i = 0;
  int j = 0;
  char temp, temp2;
  static char maze[MAX_SIZE][MAX_SIZE];
  static char path[MAX_SIZE];
  scan = scanf("%d %d\n", &x, &y);
  while (i < x) {
    scan = scanf("%c%c", &temp, &temp2);
    if (temp == 'X') {maze[i][j] = '0';}
    else {maze[i][j] = '1';}
    if ((int) temp2 == 10) {
      maze[i][j+1] = '\0';
      j = -1;
      i++;
    }
    j++;
  }
  scan = count_paths(0, 0, x, path, maze, -1, -1);
  printf("%d\n", scan);
  return scan;
}

/*
$ cat DATA.lst | ./slayfer1112
28914950
*/
