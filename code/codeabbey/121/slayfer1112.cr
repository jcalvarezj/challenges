#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 4.36 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Int32)
  data.each_line do |x|
    inter = [] of Int32
    (x.split).each do |y|
      y = y.to_i
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  sort = [array[0]]
  array = array[1..]
  counter = 0
  while array.size > 0
    sort << array[0]
    array.shift?
    n = sort.size-1
    while n > 0
      if sort[n] < sort[n-1]
        sort.swap(n,n-1)
        counter += 1
      end
      n -= 1
    end
    print "#{counter} "
    counter = 0
  end
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 1 2 3 3 4 5 4 3 2 4 9 8 1 10 12 14 1 17 14 19 19 16 10 0 7 11 11
# 9 14 15 8 6 3 14 24 16 19 34 4 0 24 5 7 36 45 13 34 35 9 33 30
# 37 37 43 3 0 57 32 18 25 26 35 39 17 61 52 8 57 41 0 30 59 70
# 34 64 48 68 11 37 19 55 49 11 35 54 75 22 77 46 89 42 39 44
# 66 10 71 69 12 29 6 47 47 94 94 48 75 59 38 53 30 109 35
# 3 69 49 11 49 26 27 109 67 97
