/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};
use std::mem;

fn distance(s1: &str, s2: &str) {
  let s1_len = s1.len() + 1;
  let s2_len = s2.len() + 1;

  let mut v0 = vec![0u8; s2_len];
  let mut v1: Vec<u8> = (0..(s2_len as u8)).collect();

  for i in 0..s1_len - 1 {
    v1[0] = i as u8 + 1;

    let mut ins_cost: u8;
    let mut sub_cost: u8;
    let mut del_cost: u8;
    for j in 0..s2_len - 1 {
      del_cost = v0[j + 1] + 1;
      ins_cost = v1[j] + 1;

      if s1.as_bytes()[i] == s2.as_bytes()[j] {
        sub_cost = v0[j];
      } else {
        sub_cost = v0[j] + 1;
      }
      if let Some(value) = vec![del_cost, sub_cost, ins_cost].iter().min() {
        v1[j + 1] = *value
      }
    }
    mem::swap(&mut v0, &mut v1);
  }
  print!("{:?} ", v0.last().unwrap_or(&0));
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut n = 0;

  for line in lines {
    let l = line.unwrap();
    if n != 0 {
      let params: Vec<&str> = l.split_whitespace().collect();
      if params[0].len() < params[1].len() {
        distance(params[1], params[0]);
      } else {
        distance(params[0], params[1]);
      }
    } else {
      n = 1;
    }
  }
  println!();
}

/*
$ cat DATA.lst | ./dfuribez
8 7 3 7 9 11 2 7 12 7 7 6 5 4 9 2 7 7 7 9 8 7 4 4 5
*/
