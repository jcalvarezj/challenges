/*
$ cargo clippy
Checking sullenestd1 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.10s
$ cargo build
Compiling sullenestd1 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.27s
$ rustc sullenestd1.rs
*/

use std::io::{self, BufRead};

fn rotate_string(k: i8, mut s: String) -> String {
  if k > 0 {
    for _ in 0..k {
      let a = s.remove(0);
      s.push(a);
    }
  } else {
    let new_k = s.len() - (-k as usize);
    for _ in 0..new_k {
      let a = s.remove(0);
      s.push(a);
    }
  }
  s
}

fn main() {
  let input = io::stdin();
  let lines = input.lock().lines();
  let mut n = 0;
  for w in lines {
    let line = w.unwrap();
    let num: Vec<String> = line
      .trim()
      .split(' ')
      .map(|w| w.parse().expect("Not a String!"))
      .collect();
    if n == 0 {
      n += 1
    } else {
      let k: i8 = num[0].parse().unwrap();
      let s = &num[1];
      println!("{}", rotate_string(k, s.to_string()));
    }
  }
}

/*
$ cat DATA.lst | ./sullenestd1
iofshyyguuaamgayuci
gswbskdntuuklhsyprqvgfp
jgjuouneoisudcaoelury
abuidycmsciwouinfoxaeth
aoiibnyjprihogkojxamyw
kqdfuitlrcuqklxuzalan
iqheqdidgoxpccaeyrok
ixzpyaypiuylfcsuap
wzmiwlkoocaeujienmgulevau
dvklsxxztdxjeifgba
*/
