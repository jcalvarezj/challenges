#!/usr/bin/env python

"""
$ pylint dfuribez.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""

import sys


def calculate_note(note_index: int, note_octave: int) -> int:
    """
    Function that calculates the frequency of a note.
    """
    return round(16.35 * (2 ** (note_index / 12)) * (2 ** note_octave))


def main() -> None:
    """
    Main function.
    """
    notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
    file = sys.stdin.readlines()
    file = list(map(str.strip, file))

    note_list = file[1].split()

    for note in note_list:
        octave = int(note[-1])
        index = notes.index(note[:-1])
        print(calculate_note(index, octave), end=" ")


main()

# $ cat DATA.lst | python dfuribez.py
# 131 49 247 52 880 104 46 110 466 494 65 92 69 185 440 659 784 156 349 277
