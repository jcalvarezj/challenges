# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength

def user_ranking_code
  folder = @config[:const][:code_dir]
  lang_info = YAML.safe_load(File.read(@config[:const][:lang_data]))
  users, totals = user_ranking_init_hashes(folder, lang_info)
  users.each_key do |user|
    sltns = Dir.glob("#{folder}/**/#{user}.*")
    sltns.each do |sltn|
      next unless check_valid_sltn_code(sltn, lang_info)
      users[user]['sltns'] += 1
      totals['sltns'] += 1
      if check_sltn_unique_code(sltn)
        users[user]['unique-sltns'] += 1
        totals['unique-sltns'] += 1
      end
      page = sltn.split('/')[1]
      unless users[user]['pages'].include?(page)
        users[user]['pages'].push(page)
        users[user]['npages'] += 1
      end
      unless totals['pages'].include?(page)
        totals['pages'].push(page)
        totals['npages'] += 1
      end
    end
  end
  users = add_percentage(users)
  [users, totals]
end
