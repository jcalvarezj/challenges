## Version 1.4.1
## language: en

Feature:
  TOE:
    bts lab
  Category:
    Access control
  Location:
    http://localhost/btslab/admin/admin.php - session (header)
    http://localhost/btslab/admin/manageusers.php - session (header)
    http://localhost/btslab/admin/MessageList.php - session (header)
  CWE:
    CWE-284: Improper Access Control
  Rule:
    REQ.023 Close inactive users sessions
    REQ.030 Avoid object reutilization
  Goal:
    Found administrator sites
  Recommendation:
    Use functions Level Access Control

  Background:
  Hacker's software:
    | <Software name>|    <Version>    |
    | Chromer        | 76.0.3809.132   |
    | Windows        | 10.0.1809 (x64) |
  TOE information:
    Given I am accessing the site http://localhost/btslab/
    And the server is running MariaDB version 10.1.36
    And PHP version 5.6.38
    And Apache version 2.4.34
    And is running on Windows 10 with XAMPP 5.6

  Scenario: Normal use case
    When I am have access to http://localhost/btslab/admin
    Then I can login as admin user

  Scenario: Static detection
    Given ToE source code
    When I inspect 'admin.php'
    Then I see the vulnerability is being caused by lines 2 to 6
    """
    2    if(!isset($_SESSION))
    3    {
    4    session_start();
    5    }
    """
    When I inspect 'manageusers.php'
    Then I see the vulnerability is being caused by lines 5 to 9
    """
    5
    6    if(isset($_POST['delete']))
    7    {
    8    mysql_query("Delete from users where username='".$_POST['user']."'");
    9    }
    """
    When I inspect 'MessageList.php'
    Then I see the vulnerability is being caused by lines 2 to 6
    """
    2    if(!isset($_SESSION))
    3    {
    4    session_start();
    5    }
    """
    And I notice that is not need be admin to do actions as admin

  Scenario: Dynamic detection
    When I am have access to http://localhost/btslab/admin/admin.php
    Then I notice that there are several administration options

  Scenario: Exploitation
    Given I know admin links
    """
    http://localhost/btslab/admin/admin.php
    http://localhost/btslab/admin/manageusers.php
    http://localhost/btslab/admin/MessageList.php
    """
    Then I perform actions like delete users or view admin messages
    Then I conclude that I can do admin actions (evidence)[evidence1.png]

  Scenario: Remediation
    Given I have patched the session fixation vulnerability using php functions
    """
    session_start();
    isset($_SESSION['isAdminLoggedIn']
    header("Location: xx.php")
    """
    And I add code in 'admin.php' as seen:
    """
    2    if(!isset($_SESSION))
    3    {
    4    session_start();
    5    }
    6    if(!isset($_SESSION['isAdminLoggedIn']))
    7    {
    8    header("Location: adminlogin.php");
    9    }
    """
    And I add code in 'manageusers.php' as seen:
    """
    5    session_start();
    6    if(!isset($_SESSION['isAdminLoggedIn']))
    7    {
    8    header("Location: adminlogin.php");
    9    }
    10    if(isset($_POST['delete']))
    11    {
    12    mysql_query("Delete from users where username='".$_POST['user']."'");
    13    }
    """
    And I add code in 'MessageList.php' as seen:
    """
    2    if(!isset($_SESSION))
    3    {
    4    session_start();
    5    }
    6    if(!isset($_SESSION['isAdminLoggedIn']))
    7    {
    8    header("Location: adminlogin.php");
    9    }
    """
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.6/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.7/10 (Medium) - CR:M/IR:M/AR:L/MAV:N/MAC:L/MPR:N/MS:C/MC:L/MI:L/MA:N/

  Scenario: Correlations
    No correlations have been found to this date 2019-09-03
