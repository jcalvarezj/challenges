## Version 1.4.1
## language: en

Feature: User-list
   TOE:
    ReconForce
  Category:
    Exposure of Resource to Wrong Sphere
  Location:
    http://192.168.1.194
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
    ('OS Command Injection')
    https://cwe.mitre.org/data/definitions/78.html
  Rule:
    REQ.173: Discard unsafe inputs
    https://fluidattacks.com/web/rules/173/
    REQ.142: Change system default credentials
    https://fluidattacks.com/web/rules/142/
  Goal:
    Get the users of the machine.
  Recommendation:
    Sanitize and validate inputs.

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Windows         | 10                |
    | Google Chrome   | 80.0.3987.132     |
  TOE information:
    Given The machine is running in virtual box
    Then The machine is using Ubuntu 19.10
    And The machine is running in "192.168.1.194"

  Scenario: Normal use case
    Given I access to "192.168.1.194" and see a button name "TroubleShoot"
    Then I click on it
    And I see that ask me a username and password
    And I don't have the credentials

  Scenario: Static detection
    Given There isn't a source code
    Then There isn't a static detection scenario either

  Scenario: Dynamic detection
    Given I access to the web and I didn't have the credentials
    When I try to found a vulnerability I try to use Nmap with the page
    Then I see that the page have 3 open ports [evidence](img1)
    And I try to use the "ftp" port
    When I open my command prompt
    Then I type ftp
    And I go to ftp menu
    When I type ftp "192.168.1.194"
    Then I receive a welcome message with
    """
    Conectado a 192.168.1.194.
    220 "Security@hackNos".
    200 Always in UTF8 mode.
    """
    And I see that ask me a username
    When I see that I try with a generic username like "ftp"
    Then I receive a message that asks me the password
    And I try with a generic password like "ftp"
    When I submit the username and password
    Then I receive a message that says "230 Login successful."
    And I see that the folder is Empty
    When I think about it I think in this '220 "Security@hackNos".'
    Then I think that this line can be a Hint
    And I try again in the "TroubleShoot" banner
    When I see that ask me a username and password
    Then I try the generic user "admin"
    And I use "Security@hackNos" in the password
    When I submit the data I see that works
    Then I was redirect to "5ecure" tab [evidence](img2)
    And I see a field to ping scan
    When I try to use this field with my IP
    Then I see the response in "5ecure/out.php"
    And I see that these field use commands
    When I try to use another command like "ls"
    Then I see I don't have any response
    And I think that probably scape some things
    When I try to avoid the scape I use a pipe "|" at the start
    Then I try with "|ls"
    And I have the files on the route [evidence](img3)
    When I see that I can conclude that the field is vulnerable
    Then I conclude a command injection vulnerability
    And I found a vulnerability

  Scenario: Exploitation
    Given I access to the "5ecure" tab
    When I try to think a way to exploit this vulnerability
    Then I want to get the users of the machine
    And I use the command injection for that Goal
    When I type "|cat /etc/passwd"
    Then I get the response in "5ecure/out.php"
    And I can see the users list [evidence](img4)

  Scenario: Remediation
    Given A command execution without improper scape
    Given I didn't have the source code
    When I think in a lot of ways to fix the vulnerability
    Then I can use "escapeshellcmd" and "escapeshellarg" in "out.php"
    """
    exec(escapeshellcmd("ping " . escapeshellarg($_GET['id'])), $output);
    """
    And I can fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:H/PR:L/UI:N/S:U/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - E:H/RL:O/RC:C/CR:H/IR:H/AR:H
  Environmental: Unique and relevant attributes to a specific user environment
    7.2/10 (High) - MAV:N/MAC:H/MPR:L/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2020-04-14
