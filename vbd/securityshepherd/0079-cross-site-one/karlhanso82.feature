## Version 2.0.1
## language: en

Feature:
  TOE:
    securityshepherd
  Category:
    unreliable data
  Location:
    https://localhost/lessons/
    zf8ed52591579339e590e0726c7b24009f3ac54cdff1b81a65db1688d86efb3a.jsp
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.160 Encode system outputs
  Goal:
    Exploit the xss vulnerability to gain the challenge key
  Recommendation:
    Use a library to circumvent this kind of malicious inputs

 Background:
 Hacker's software:
   | <software name>      | <version>      |
   | Microsoft Windows 10 | 10.0.17763.437 |
   | Docker               | 2.0.0.3        |
 TOE information:
   Given I'm entering the site https://localhost/
   And I access a site which has been build in java and mysql

 Scenario: Normal use case
   Given I enter https://localhost/
   Then the browser redirect me to https://localhost/login.jsp
   Then I enter the user credentials given by OWASP Security Shepherd
   """
   https://github.com/OWASP/SecurityShepherd
   """
   Then I go to the site and click on the link Get the next Challenge
   And later I saw  an input search term that says enter the term
   And an input with the name get this user
   And an input to submit the challenge key

Scenario: Static detection
  Given I enter https://localhost/login.jsp
  And I click to the next challenge link
  Then I saw that the site is build on java and I have access
  And I just look for webapp directory where tomcat deploy the apps
  Then I find ROOT.war and unzip it
  And I see the folder
  Then I saw link with the id of challenge
  """
  https://localhost/challenges/
  d72ca2694422af2e6b3c5d90e4c11e7b4575a7bc12ee6d0a384ac2469449e8fa.jsp
  """
  Then I saw the snippet of code that triggers the post request
  """
  86  var theSearchTerm = $("#searchTerm").val();
  87  $("#resultsDiv").hide("slow", function(){
  88        var ajaxCall = $.ajax({
  89        dataType: "text",
  90        type: "POST",
  91 url: "d72ca2694422af2e6b3c5d90e4c11e7b4575a7bc12ee6d0a384ac2469449e8fa",
  92    data: {
  93   searchTerm: theSearchTerm,
  94    csrfToken: "<%= csrfToken %>"
  95   },
  96   async: false
  97   });
  """
  And given these clues I decided to look the backend code
  """
  String searchTerm = request.getParameter("searchTerm");
        log.debug("User Submitted - " + searchTerm);
        searchTerm = XssFilter.levelOne(searchTerm);
        log.debug("After Filtering - " + searchTerm);
        String htmlOutput = new String();
        if(FindXSS.search(searchTerm))
        {
          htmlOutput = "<h2 class='title'>"
          + bundle.getString("result.wellDone") + "</h2>" +
              "<p>" + bundle.getString("result.youDidIt") + "<br />" +
              bundle.getString("result.resultKey") + " <a>" +
                Hash.generateUserSolution(
                     Getter.getModuleResultFromHash(
                    getServletContext().getRealPath(""), levelHash
                    ), (String)ses.getAttribute("userName")
                )
              +
              "</a>";
        }
  """
  Then I spot searchTerm var is not sanitized

Scenario: Dynamic detection
  Given I enter https://localhost/login.jsp
  And I click the get the next challenge link
  Then I saw the description
  """
  Find a XSS vulnerability in the following form.
  It would appear that your input is been filtered!
  Please enter the Search Term that you want to look up
  """
  Then I saw an input box with the name search term
  And a button with the name get this user
  And I peek closely if the input handles some validation
  Then I see that input is not sanitized at the form data
  """
  searchTerm: <script>test</script>
  csrfToken: -42855771674222937537327002538021009510
  """
  And I conclude the static detection

Scenario: Exploitation
  Given the challenge link
  Then I send a xss code to the searchterm input
  """
  <INPUT TYPE="BUTTON" ONCLICK="alert('XSS')"/>
  """
  Then it displays they key on the same page of the challenge
  And inject a checkbox and I checked and show me the flaw
  And these kind of shortcomings lead to session hijacking
  And also lead to web defacement

Scenario: Remediation
  When the page has been developed
  Then it should escape these special characters
  And these kind of injection attacks changes the meaning of the comands
  And a way to prevent those is to use a framework
  And another way is Java Encoder Project from OWASP like the code below
  """
  import org.owasp.encoder.Encode;
  String searchTerm =  Encode.forHtml(request.getParameter("searchTerm"));
        log.debug("User Submitted - " + searchTerm);
        searchTerm = XssFilter.levelOne(searchTerm);
        log.debug("After Filtering - " + searchTerm);
        String htmlOutput = new String();
        if(FindXSS.search(searchTerm))
        {
          htmlOutput = "<h2 class='title'>"
          + bundle.getString("result.wellDone") + "</h2>" +
              "<p>" + bundle.getString("result.youDidIt") + "<br />" +
              bundle.getString("result.resultKey") + " <a>" +
                 Hash.generateUserSolution(
                    Getter.getModuleResultFromHash(
                    getServletContext().getRealPath(""), levelHash
                    ), (String)ses.getAttribute("userName")
                )
              +
              "</a>";
        }
  """
  Then this going to halt this injection in the backed code

Scenario: Scoring
 Severity scoring according to CVSSv3 standard
 Base: Attributes that are constants over time and organizations
   6.1/10 (Low) - AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N
 Temporal: Attributes that measure the exploit's popularity and fixability
   6.1/10 (Low) - E:F/RL:O/RC:R
 Environmental: Unique and relevant attributes to a specific user environment
   6.1/10 (Medium) - CR:L/IR:L/AR:L

 Scenario: Correlations
   No correlations have been discovered to this date 2019-05-24