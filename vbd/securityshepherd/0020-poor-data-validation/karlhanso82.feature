## Version 2.0.1
## language: en

Feature:
  TOE:
    securityshepherd
  Category:
    Improper data input handling
  Location:
    http://localhost/
    4d8d50a458ca5f1f7e2506dd5557ae1f7da21282795d0ed86c55fefe41eb874f.jsp
  CWE:
    CWE-020: Improper Input Validation
  Rule:
    REQ.176: Restrict system objects
  Goal:
    Get the result key given the improper data validation
  Recommendation:
    For every input data request
    Asumme all of those are malicious
    And check the type and use whitelist of acepted data

 Background:
 Hacker's software:
   | <software name>      | <version>      |
   | Microsoft Windows 10 | 10.0.17763.437 |
   | OWASP ZAP            | 2.7.0          |
 TOE information:
   Given I'm entering the site https://localhost/
   And I access a site which has been build in java and mysql

 Scenario: Normal use case
   Given I enter https://localhost/
   Then the browser redirect me to https://localhost/login.jsp
   Then I enter the user credentials
   And I enter the site and click on the link Get the next Challenge

 Scenario: Static detection
   Given I enter enter https://localhost/login.jsp
   And I click to the next challenge link
   Then I saw that the site is build on java and I have access
   And I just look for webapp directory where tomcat deploy the apps
   Then I find ROOT.war and unzip it
   And I see the folder
   Then I saw link with the id of challenge
   """
   4d8d50a458ca5f1f7e2506dd5557ae1f7da21282795d0ed86c55fefe41eb874f.jsp
   """
   Then I saw the code does not ensure some data verifications in the front end
   And send it a plain data
   """
   127 var ajaxCall = $.ajax({
   128    type: "POST",
   129    url: "<%= levelHash %>",
   130    data: {
   131    userdata: number
   132    },
   133    async: false
   134    });
   135     if(ajaxCall.status == 200)
   136    {
   137     $("#resultsDiv").html(ajaxCall.responseText);
   138    }
   """
   And since is a jsp page without backend code
   Then I search for the post request levelhash
   And it is the id of the challenge
   Then I search for it in web.xml
   And it leads to PoorValidationLesson.class
   Then the input is not handled properly in that class
   And I finish the inspection

Scenario: Dynamic detection
   Given I enter https://localhost/login.jsp
   And I click the get the next challenge link
   Then I saw the description
   """
   Poor Data Validation occurs when an application does not
   validate submitted data correctly or sufficiently.
   Poor Data Validation application issues are generally low severity,
   they are more likely to be coupled with other security risks to increase
   their impact. If all data submitted to an application is validated
   correctly, security risks are significantly more difficult to exploit
   """
   And I saw an input where i must send a key
   Then I refresh the page and I open the brower console
   And I see post request that sends userdata=1
   Then I see that parameter is not handled properly

Scenario: Exploitation
   Given the challenge link we send data to the input
   Then we use OWASP zap tool to record the post request
   And I see this post request
   Then I saw userdata=1
   And we modify this parameter to username=-1
   Then we recieve the result key
   And it leas to arbitrary code execution and altered control flow

 Scenario: Remediation
   When the page has been developed
   Then it should be ensure that the inputs are checked
   And data validated
   Then that solves the issue

 Scenario: Scoring
 Severity scoring according to CVSSv3 standard
 Base: Attributes that are constants over time and organizations
   6.7/10 (Low) - AV:L/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:H
 Temporal: Attributes that measure the exploit's popularity and fixability
   6.7/10 (Low) - E:F/RL:O/RC:R
 Environmental: Unique and relevant attributes to a specific user environment
   6.7/10 (Medium) - CR:L/IR:L/AR:L

 Scenario: Correlations
   No correlations have been discovered to this date 2019-05-14