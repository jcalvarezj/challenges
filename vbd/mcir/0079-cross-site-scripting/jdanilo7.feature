## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR: Magical Code Injection Rainbow
  Category:
    Cross-Site Scripting
  Location:
    MCIR/xssmh/challenges/challenge0.php - Injection String (field)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting') -base-
      https://cwe.mitre.org/data/definitions/79.html
    CWE-0074: Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection') -class-
      https://cwe.mitre.org/data/definitions/74.html
    CWE-0990: SFP Secondary Cluster: Tainted Input to Command -category-
      https://cwe.mitre.org/data/definitions/990.html
  CAPEC:
    CAPEC-591: Reflected XSS -detailed-
      http://capec.mitre.org/data/definitions/591.html
    CAPEC-063: Cross-Site Scripting (XSS) -standard-
      http://capec.mitre.org/data/definitions/63.html
    CAPEC-242: Code Injection -meta-
      http://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Disclose the contents of the site cookies
  Recommendation:
    Escape the input

  Background:
  Hacker's software:
    |     <Software name>      |    <Version>    |
    |          OWASP           |       1.6       |
    |   VMWare Workstation 15  |     15.0.2      |
    |     Firefox Quantum      |    60.2.0esr    |
    |        Kali Linux        |      4.18       |
  TOE information:
    Given I'm running OWASP on VMWare on Kali Linux
    And I'm accessing the MCIR pages through my Firefox browser
    And I enter a php page that takes a string and then displays it

  Scenario: Normal use case
  The site allows the user to enter a string that is later shown on the page
    Given I access the page MCIR/xssmh/challenges/challenge0.php
    And I enter the string "hello"
    And I click the Inject! button
    Then the string is shown as part of the site's content
    And I can see [evidence](normal-use-case.png)

  Scenario: Static detection
  The input is not validated before being included in the html source code
    When I look at the source code
    And I check the code that prepares the XML request
    """
    43  $base_output = 'Foo! <img src="baz.jpg"><input type="text"
                             value="bar!"><script>a="javascript";</script>';
    ...
    54  switch ($_REQUEST['location']){
    55    case 'condition_string':
    56      $output = str_replace('Foo!', $_REQUEST['inject_string'],
                     $base_output);
    57      break;
    ...
    99  echo '<b>Output:</b><br>' . $output;
    """
    Then I notice there is no validation on the input

  Scenario: Dynamic detection
  The input of the Inject! field is added to the html source code
    Given I enter "<script>alert('hi');</script>" in the text field
    And I click the Inject! button
    Then I get an alert window that can be seen in [evidence](alert.png)
    And I conclude that my input was treated as code by the server
    And the application is vulnerable to cross-site scripting

  Scenario: Exploitation
  Retrieving all cookies
    Given I enter the following code in the input field
    """
    1  <script>
    2  document.cookie = "username=John Doe";
    3  var x = document.cookie;
    4  alert(x);
    5  </script>
    """
    And I click the Inject! button
    Then I get an alert window showing all the cookies
    And it is exemplified by the cookie I inserted
    And it can be seen in [evidence](cookies.png)

  Scenario: Remediation
  The php code can be fixed by escaping the input before displaying it
    Given I have patched the code by escaping the user input
    """
    99  echo '<b>Output:</b><br>' . htmlspecialchars($output, ENT_QUOTES);
    """
    Then I can prevent the injection of the code shown above
    Given I re-run my exploit
    Then I get no alert window and my input is shown without getting executed
    And it can be seen in [evidence](escaped-output.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.3/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.9/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.6/10 (High) - CR:M/IR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-12-19
