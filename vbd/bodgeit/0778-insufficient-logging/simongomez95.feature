## Version 1.4.2
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Improper Error Handling
  Location:
    /bodgeit/login - Logs
  CWE:
    CWE-0778: Insufficient Logging -base-
      https://cwe.mitre.org/data/definitions/778.html
    CWE-0693: Protection Mechanism Failure -class-
      https://cwe.mitre.org/data/definitions/693.html
    CWE-0254: 7PK - Security Features
      https://cwe.mitre.org/data/definitions/254.html
  CAPEC:
    No CAPEC attack patterns related to this vulnerability
  Rule:
    REQ.075: https://fluidattacks.com/web/es/rules/075/
  Goal:
    Provoke an error in the system without leaving traces
  Recommendation:
    Don't return system errors to the user

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "bodgeit/login.jsp"
    Then I get a login page where I can submit credentials

  Scenario: Static detection
  No origin validation
    Given I check the configuration at
    """
    bodgeit/root/login.jsp
    """
    And I see
    """
    ...
    60  } catch (Exception e) {
        61  if ("true".equals(request.getParameter("debug"))) {
        62      stmt.execute("UPDATE Score SET status = 1 WHERE task = 'HIDDEN_D
        EBUG'"
    );
        63      out.println("DEBUG System error: " + e + "<br/><br/>");
        64  } else {
        65      out.println("System error.");
        66  }
    ...
    """
    Then I notice it's not logging failed login attempts

  Scenario: Dynamic detection
  No dynamic detection posible for this vulnerability

  Scenario: Exploitation
  No logging no traces
    Given I perform a brute force attack against the login system
    Given there are no logs
    Then it's not possible to investigate my attack afterwards

  Scenario: Remediation
  Implement logging on security events
    Given I implement a logging system to the application
    And make it log anytime something potentially dangerous happens
    Then it's easier to conduct forensics after an attack attempt

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:P/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0089-*
      Given I am attempting a SQL injection attack
      Then I'm provoking many errors in the system
      But if my requests are not logged
      Then I won't be caught
