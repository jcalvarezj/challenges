## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Location:
    http://172.17.0.2/?page=editprofile - id (field)
  CWE:
    CWE-200: Exposure of Sensitive Information to an Unauthorized Actor
  Rule:
    REQ.176: R176. Restrict system objects
  Goal:
    Get a list of all registered members
  Recommendation:
    Check user's privileges

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Firefox         | 78.0.2      |
    | Python          | 3.8.3       |
  TOE information:
    Given I am accessing the site at http://172.17.0.2/
    And it runs on a docker container

  Scenario: Normal use case
    Given I am logged in
    When I go to
    """
    http://172.17.0.2/?page=editprofile
    """
    Then I can modify my profile [evidence](normal.png)

  Scenario: Static detection
    When I look at the code at "editprofile.php"
    Then I can see that the code only checks for a logged user
    """
    3 if(!isset($_SESSION['user_id'])) {
    4   echo getForbiddenMessage();
    5   return;
    6 }
    """
    Then I can conclude I can get access to other user's data

  Scenario: Dynamic detection
    Given I am logged
    And in the "profile" section
    When I go to
    """
    http://172.17.0.2/?page=editprofile&id=1
    """
    Then I can see admin's info [evidence](admin.png)
    And I can conclude that with some bruteforce I can get all users

  Scenario: Exploitation
    Given now I know the system is vulnerable
    And I code "bruteforce.py" to get all user's info
    When I run the script looking for the first 100 ids:
    """
    $ python bruteforce.py 100
    """
    Then I get a list of users
    """
    [+] id:1  admin  qwe@qwer.com    Germany
    [+] id:2  user   asd@asd.asd     Holy See (Vatican City State)
    [+] id:4  test3  test3@test.com  Afghanistan
    """
    And I can conclude that I can leak all user's mails and locations

  Scenario: Remediation
    Given I have patched the code by doing adding
    """
    08  if (isset($_GET["id"]) &&
    09    $_GET["id"] != $_SESSION['user_id'] &&
    10    $_SESSION['user']['is_admin'] != 1) {
    11      error(500, "DIWA Error 500");
    12  }
    """
    And line 9 checks if the "id" requested is equal to the "id" of the user
    And line 10 makes sure any admin can see that content
    When I run again the script
    """
    $ python bruteforce.py 100
    """
    Then I only get my own data
    """
    [+] id:2  user  user@example.com  Germany
    """
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.0 (Medium) - AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.8 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.8 (Medium) - MAV:N/MAC:L/MPR:L/MUI:N/MS:C/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date {yyyy-mm-dd}
