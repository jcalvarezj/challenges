## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Credentials Management
  Location:
    webgoat-lessons/challenge/src/main/java/org/owasp/webgoat/plugin/SolutionCon
    stants.java - PASSWORD, PASSWORD_TOM, ADMIN_PASSWORD_LINK (variables)
  CWE:
    CWE-798: Use of Hard-coded Credentials
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Obtain credentials from code file
  Recommendation:
    Don't burn sensitive data in code

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    There is no normal use case

  Scenario: Static detection
  No origin validation
    Given I check the code at
    """
    webgoat-lessons/challenge/src/main/java/org/owasp/webgoat/plugin/SolutionCon
    stants.java
    """
    And I see
    """
    public interface SolutionConstants {

        //TODO should be random generated when starting the server
        String PASSWORD = "!!webgoat_admin_1234!!";
        String PASSWORD_TOM = "thisisasecretfortomonly";
        String ADMIN_PASSWORD_LINK = "375afe1104f4a487a73823c50a9292a2";
    }
    """
    Then I see secrets in the code
    And I cry

  Scenario: Dynamic detection
  There is no way to dynamically detect this by itself

  Scenario: Exploitation
  Using credentials found in code
    Given I have obtained a copy of the project code
    Then I find the credentials burned in the code
    Then I can use this creds in the live site
    And get access I shouldn't have

  Scenario: Remediation
  Check the origin corresponds to the site
    Given I remove the credentials from the code
    And change them all
    And safely store them in a database
    Then they aren't in the codebase anymore
    And can't be found by an evil actor

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.0/10 (High) - AV:L/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.4/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.4/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    611-xxe
    502-insecure-deserialization
    89-sql-inyection
    89-without-password
      Given I use an RCE bug or SQLi to read the credentials file
      Then I have the creds
      And can use them in the live site
