## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Category:
    Improper Authentication
  Location:
    http://testphp.vulnweb.com/login.php - user,password
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.229: https://fluidattacks.com/web/rules/229/
  Goal:
    Gain access using brute force
  Recommendation:
    Limit the number of login attempts

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 60.8.0      |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com/
    And enter a php site

  Scenario: Normal use case
    Given I access http://testphp.vulnweb.com/
    When I click on "Your Profile"
    Then the webpage redirects me to the login window.

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection.

  Scenario: Dynamic detection
    Given I access http://testphp.vulnweb.com/login.php
    When I try to log in using random usernames and passwords
    Then I get redirected to the same page with no error message
    And I can conclude that there is no login attempt limit.

  Scenario: Exploitation
    Given I access http://testphp.vulnweb.com/login.php
    When I try to log in with ramdom usernames and passwords
    Then I see that the website does not have a login attempt limit
    And I can start testing username/password combinations
    When I try to log in using "test" as username and "test" as password
    Then I see I have successfuly logged in
    And I can conclude that the website is vulnerable to brute force attacks.

  Scenario: Remediation
    When the website is being developed a login attempt limit should be added
    Then the website would no longer be vulnerable to added brute force attacks

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-08-16
