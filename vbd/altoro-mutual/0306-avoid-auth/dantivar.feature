## Version 1.4.1
## language: en

Feature:
  TOE:
    Altoro Mutual
  Location:
    https://demo.testfire.net/login.jsp
  CWE:
    CWE-306: Missing Authentication for Critical Function
  Rule:
    REQ.R264: R264. Request authentication
  Goal:
    Gain access to other users accounts
  Recommendation:
    Implement authentication for critical requests

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Debian          |   stretch   |
    | Chrome          |      84     |
  TOE information:
    Given I am accessing the TOE

  Scenario: Normal use case
    Given I access the site login page
    And I have my credentials
    Then I use them to login to my account

  Scenario: Static detection
    Given I don't have access to the source code

  Scenario: Dynamic detection
    Given that I have any valid credentials
    Then I try to check my account
    And I see that my account number is passed as a parameter
    And is visible in the address bar
    Then I try to put random account numbers in the parameter

  Scenario: Exploitation
    Given that I know I can give account numbers as a parameter
    Then I try to send an account number that is not mine
    When I do it I see the information about that account movements
    Then I conclude I can access others user account movements
    And it can be done just by having their account number

  Scenario: Remediation
    Given the scenario
    Then it should be requested authentication to access sensitive information

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.4/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date {2020-08-20}
