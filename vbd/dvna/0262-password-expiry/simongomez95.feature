## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Credentials Management
  Location:
    / - Credentials
  CWE:
    CWE-0262: Not Using Password Aging -base-
      https://cwe.mitre.org/data/definitions/262.html
    CWE-0287: Improper Authentication -class-
      https://cwe.mitre.org/data/definitions/287.html
    CWE-1028: Broken Authentication -category-
      https://cwe.mitre.org/data/definitions/1028.html
  CAPEC:
    CAPEC-016: Dictionary-based Password Attack -detailed-
      http://capec.mitre.org/data/definitions/16.html
    CAPEC-049: Password Brute Forcing -standard-
      http://capec.mitre.org/data/definitions/49.html
    CAPEC-112: Brute Force -meta-
      http://capec.mitre.org/data/definitions/112.html
  Rule:
    REQ.130: https://fluidattacks.com/web/en/rules/130/
  Goal:
    Better credential security
  Recommendation:
    Expire passwords after a set time

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No static detection

  Scenario: Dynamic detection
  Same password forever
    Given I create an account
    And never change my password
    Then I can always use the same password

  Scenario: Exploitation
  Finding functioning old passwords
    Given I have a combolist for users of the application
    And some user never changed their password
    Then I have access to their account

  Scenario: Remediation
  Set password expiry time
    Given I set a password expiry time of 60 days
    Then users have to change their pass periodically
    Then compromised passwords are less likely to work

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.8/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.3/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-29
